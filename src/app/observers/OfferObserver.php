<?php

// Core
use Carbon\Carbon;

// Composer
use Uuid;

class OfferObserver
{
    ////////////////////////
    // Watch Model events //
    ////////////////////////


    /**
     * On updating an offer, make sure the state is sane (offers are created with
     * nothing but a price, hence this all happening in the updated event)
     *
     * @param  Offer $offer
     */
    public function updating($offer)
    {
        // Status - pending status as a base
        $offer->acceptance_status = $offer->acceptance_status ?: Offer::OFFER_PENDING;

        // Only one offer can be accepted at a time - cancel updates if
        // this is an attempt to accept any offer once one has been accepted
        if ($offer->acceptance_status == Offer::OFFER_ACCEPTED
                && $offer->advert->winning_offer !== false) {
            return false;
        }
        
        // Auto decline status if below threshold
        $offer->acceptance_status = $offer->offer_price < $offer->advert->min_offer_price
            ? Offer::OFFER_DECLINED_AUTO
            : $offer->acceptance_status;

        // Expiry - set to one year in the future
        $offer->expires_at = Carbon::now()->addYear();

        // Acceptance code
        $offer->acceptance_code = $offer->acceptance_status == Offer::OFFER_ACCEPTED
            ? $offer->acceptance_code = Uuid::generate()->string
            : $offer->acceptance_code = null;
    }


    /**
     * After saving, update reservation status on the advert if appropriate
     * and auto decline all other offers
     *
     * @param  Offer $offer
     */
    public function saved($offer)
    {
        if ($offer->acceptance_status == Offer::OFFER_ACCEPTED) {

            $offer->advert->fill(['reserved_at' => Carbon::now()])->save();
            
            $other_offers = $offer->advert->offers()->where('id', '!=', $offer->id)->get();
            foreach ($other_offers as $other_offer) {
                $other_offer->fill(['acceptance_status' => Offer::OFFER_DECLINED_AUTO])->save();
            }
        }
    }
}
