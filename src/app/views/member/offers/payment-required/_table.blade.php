<table class="table table-striped data-table" id="member-offers-paymentrequired">

	<thead>
		<tr>
			@if (Request::ajax())
				<th class="table-widget-fixed-column">{{-- Empty controls placeholder --}}</th>
			@endif
			<th>{{-- Advert Title --}}</th>
			<th>Price</th>
			<th>Made</th>
			<th data-orderable="false">{{-- Buy Now --}}</th>
		</tr>
	</thead>
	<tbody>
	
		@foreach($payment_required as $offer)

				<tr>
					@if (Request::ajax())
						<td>{{-- Empty controls placeholder --}}</td>
					@endif
					<td>{{ link_to_route('public.advert.archived', $offer->advert->title, ['slug' => $offer->advert->slug], ['target' => '_blank']) }}
</td>
					<td>{{ $offer->gbp_offer_price_string }}</td>
					<td>{{ $offer->created_at->diffForHumans() }}</td>
					<td>{{ link_to_route('offer.redeem', 'Complete Payment', [
							'offer' => $offer->acceptance_code,
							])
					}}</td>
				</tr>

		@endforeach

	</tbody>
</table>