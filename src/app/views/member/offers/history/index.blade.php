@extends('member.layout')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h1>Purchase History</h1>
			</div>

			<div class="panel-body">

				@if (! $purchases->isEmpty())

					@include('member.offers.history._table', $purchases)
					
				@else

					<div class="well">
						None! Why not {{ link_to_route('home', 'find yourself something!') }}
					</div>

				@endif

			</div> {{-- /.panel-body --}}
		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop
