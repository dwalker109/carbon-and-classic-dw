@extends('member.layout')

@section('title', 'All Purchases')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h1>All Purchases</h1>
			</div>

			<div class="panel-body">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#awaiting-payment" aria-controls="awaiting-payment" role="tab" data-toggle="tab">Awaiting Payment</a></li>
					<li role="presentation"><a href="#purchase-history" aria-controls="purchase-history" role="tab" data-toggle="tab">Purchase History</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">

					<div role="tabpanel" class="tab-pane fade in active" id="awaiting-payment">

						@if ($offers->get('awaiting_payment')->isEmpty())

							<div class="well">
								<p>You don't have any outstanding payments</p>
							</div>

						@else

							@include('member.offers.payment-required._table', [
								'payment_required' => $offers->get('awaiting_payment')
							])

						@endif
						
					</div>{{-- ./tab-pane --}}
					
					<div role="tabpanel" class="tab-pane fade" id="purchase-history">

						@if ($offers->get('purchase_history')->isEmpty())

							<div class="well">
								<p>You don't have any completed purchases to display.</p>
							</div>

						@else

							@include('member.offers.history._table', [
								'purchases' => $offers->get('purchase_history')
							])

						@endif

					</div>{{-- ./tab-pane --}}
				
				</div>{{-- ./tab-content --}}

				{{ link_to_route('member.dashboard', 'Return to Dashboard', null, ['class' => 'btn btn-primary']) }}
				
			</div> {{-- /.panel-body --}}
		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop
