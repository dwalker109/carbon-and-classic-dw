@extends('member.layout')

@section('title', "Summary for {$advert->title}")

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h2>{{ $advert->title }}</h2>
				<p>Added {{ $advert->created_at->diffForHumans() }}</p>
			</div>

			<div class="panel-body">

				<div class="row">

					{{-- Show winner/purchase details if appropriate --}}
					@unless ($advert->winning_offer === false)

						<div class="col-xs-12">

							<h2>Purchaser</h2>

							<div class="alert alert-success">

								<p class="lead">
									{{ link_to_route('messages.create', "Message buyer", $advert->winning_offer->initiator->username, 
										['class' => 'btn btn-xs btn-info pull-right']) 
									}}
									This advert was purchased for 
									{{ $advert->winning_offer->gbp_offer_price_string }}
									by <strong>{{ $advert->winning_offer->initiator->username }}</strong>
									@if ($advert->winning_offer->initiator->profile->full_name)
										({{ $advert->winning_offer->initiator->profile->full_name }})
									@endif
									@if ($advert->winning_offer->initiator->profile->telephone_country)
										from {{ $advert->winning_offer->initiator->profile->country->country_name }}
									@endif
									{{ $advert->winning_offer->created_at->diffForHumans() }}
								</p>

							</div>

							<h2>PayPal</h2>

							@if ($advert->winning_transaction)

								<div class="alert alert-success">

									<p class="lead">
										@if (! $advert->shipped_at)

											{{ link_to_route('member.listings.shipped', 'Mark shipped', [$advert->id],
												['class' => 'btn btn-xs btn-info pull-right'])
											}}

										@endif
										The item was paid for 
										{{ $advert->winning_transaction->updated_at->diffForHumans() }}
										from PayPal ID
										{{ $advert->winning_transaction->paypal_sender_email }}
									</p>

								</div>

							@else

								<div class="alert alert-danger">

									<p class="lead">
										The item has not yet been paid for, so do not ship until payment 
										has been received in your PayPal account!
									</p>

								</div>

							@endif


							@if ($advert->shipped_at)

								<h2>Shipping</h2>

								<div class="alert alert-success">

									<p class="lead">It was marked as shipped
										{{ $advert->shipped_at->diffForHumans() }}
									</p>

								</div>

							@endif

						</div>

					@endunless

					<div class="col-sm-6">

						<h3>Pending offers</h3>
						@if ($advert->offers()->pending()->count())

							<div class="alert alert-info">
								@foreach ($advert->offers()->pending()->get() as $offer)
									<p>
										<strong>{{ $offer->gbp_offer_price_string }}</strong>
										from <span class="text-muted">{{ $offer->initiator->username }}</span>
									</p>

									{{-- Accept and decline --}}
									{{ link_to_route('member.offers.accept', 'Accept', [$offer->id], ['class' => 'btn btn-xs btn-success']) }}
									{{ link_to_route('member.offers.decline', 'Decline', [$offer->id], ['class' => 'btn btn-xs btn-danger']) }}

								@endforeach
							</div>
						
						@else

							<div class="alert alert-warning" role="alert">
								<p>None</p>
							</div>
						
						@endif

					</div>

					<div class="col-sm-6">
						<h3>Actioned offers</h3>

						@if ($advert->offers()->actioned()->count())

							<div class="alert alert-info">
								@foreach ($advert->offers()->actioned()->get() as $offer)
									<p>
										<strong>{{ $offer->gbp_offer_price_string }}</strong>
										from <span class="text-muted">{{ $offer->initiator->username }}</span>,
										<span class="text-success">{{ $offer->acceptance_status_string }}</span>
									</p>
								@endforeach
							</div>

						@else

							<div class="alert alert-warning" role="alert">
								<p>None</p>
							</div>

						@endif

					</div>


				</div> {{-- ./row --}}

				<div class="text-center">
					{{ link_to_route('member.dashboard', 'Return to dashboard', 
						null, ['class' => 'btn btn-primary'])
					}}
				</div>

			</div> {{-- /.panel-body --}}
		</div> {{-- /.panel-default --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop
