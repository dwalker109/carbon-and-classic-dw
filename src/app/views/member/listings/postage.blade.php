@extends('member.listings._form')

@section('title', "Postage options for {$advert->title}")

@section('main')

{{-- Retrieve the custom error messages - this view is non-standard --}}
<?php $postage_errors = Session::get('postage_errors', new Collection()) ?>

<div class="panel-heading">
	<h1>Postage Options</h1>
	<p>Tick the options you wish to offer, then complete the price of
	each along with a brief description of the service.</p>
	<ul>
		<li class="text-info">Editing postage options will <b>not</b> require your advert
		be reapproved, so feel free to amend these as much as you need.</li>
		<li class="text-info">To offer <b>free</b> postage on any of the options, just enter
		<b>0</b> in the price field.</li>
	</ul>
</div>

<div class="panel-body">

	{{ Former::framework('Nude') }}
	{{ Former::open(route('member.listings.update_postage', $advert->id))->method('POST') }}
	{{ Former::populate(['postage_options' => $regions->toArray()]) }}

		<div class="row">
		<div class="col-xs-12">
		<table class="table table-striped table-responsive">

		@foreach ($regions as $region => $model)

			{{-- Retrieve errors --}}
			@if ($postage_errors->has($region))
				<?php $region_errors = Collection::make($postage_errors->get($region)->getMessages()) ?>
			@else
				<?php $region_errors = new Collection() ?>
			@endif

			<tr>

				<td width="20%">

					{{ Form::label("postage_options[{$region}][enabled]", $region, ['class' => 'form-label']) }}
					{{ Form::checkbox("postage_options[{$region}][enabled]", 'enabled', $checked = isset($model->enabled) ? true : false) }}

					<input name="postage_options[{{ $region }}][region_name]" type="hidden" value="{{ $region }}">

					@if ($region_errors->has('region_name'))
						<p class="text-danger">{{ head($region_errors->get('region_name')) }}</p>
					@endif
				</td>

				<td width="25%">
					@if ($region == PostageOption::COLLECT_IN_PERSON)
						<p class="form-control-static">{{ 
							PostageOption::$collect_in_person_prototype['price'] ?: 'Free of charge' 
						}}</p>
					@else
						{{ Former::input("postage_options[{$region}][price]", '')->placeholder('Price (£)')->class('form-control') }}
					@endif
					
					@if ($region_errors->has('price'))
						<p class="text-danger">{{ head($region_errors->get('price')) }}</p>
					@endif
				</td>

				<td>
					@if ($region == PostageOption::COLLECT_IN_PERSON)
						<p class="form-control-static">{{ 
							PostageOption::$collect_in_person_prototype['description']
						}}</p>
					@else
						{{ Former::input("postage_options[{$region}][description]", '')->placeholder('Notes for this postage option')->class('form-control') }}
					@endif
					
					@if ($region_errors->has('description'))
						<p class="text-danger">{{ head($region_errors->get('description')) }}</p>
					@endif
				</td>

			</tr>

		@endforeach

		</table>
		</div> {{-- ./col-xs-12 --}}
		</div> {{-- ./row --}}

		<div class="text-center">
			{{ Form::submit('Save postage options', ['class' => 'btn btn-primary']) }}
			{{ link_to_route('member.listings.interstitial', 'Cancel', 
				['id' => $advert->id], ['class' => 'btn btn-primary']) }}
		</div>

	{{ Former::close() }}

</div> {{-- /.panel-body --}}

@stop
