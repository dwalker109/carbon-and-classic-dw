{{ Former::populate($advert) }}

{{ Former::text('title') }}
{{ Former::textarea('body')->rows(10) }}

{{ Former::text('price')->pattern('[-+]?[0-9]*[.,]?[0-9]+')->placeholder('&pound;') }}

<?php $reserve_tooltip_markup = <<<HTML
<abbr title="Any offers made below this value will be declined automatically, though you will still be able to see the declined offers in your dashboard. This value is never displayed to potential buyers.">
	Reserve Price
</abbr>
HTML;
?>

{{ Former::text('min_offer_price', $reserve_tooltip_markup) ->pattern('[-+]?[0-9]*[.,]?[0-9]+')->placeholder('&pound;') }}

{{ Former::select('category_id', 'Category')->options($categories)->placeholder('Please select')->addClass('hierarchy not-select2') }}
{{ Former::select('manufacturer_id', 'Manufacturer')->options($manufacturers)->placeholder('Please select') }}

<span id="mfr_other_freetext_container">
	{{ Former::text('mfr_other_freetext', ' ')->placeholder('Enter manufacturer here') }}
</span>

{{ Former::actions(
	Button::primary('Save')->submit(),
	Button::danger('Cancel')->asLinkTo(route('member.listings.index'))
) }}