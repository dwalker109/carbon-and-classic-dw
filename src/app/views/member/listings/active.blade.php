@extends('member.layout')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h1>Active Listings</h1>
				<a class="btn btn-default" href="{{ route('member.listings.create') }}" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New Advert</a>
			</div>

			<div class="panel-body">

				@if ($adverts->isEmpty())

					<div class="well">
						<p>You haven't listed any items for sale at the moment. Why not 
						{{ link_to_route('member.listings.create', 'list one right now?') }}
						</p>
					</div>

				@else

					@include('member.listings._table', compact('adverts'))

				@endif

			</div> {{-- /.panel-body --}}
		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop
