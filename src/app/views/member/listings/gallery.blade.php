@extends('member.listings._form')

@section('title', "Manage gallery for {$advert->title}")

@section('main')

<div class="panel-heading">
	<h1>Manage gallery for {{ $advert->title }}</h1>
</div>

<div class="panel-body">

	<div class="row">

		<div class="col-sm-6">

			<h2>Upload pictures</h2>
			<p class="text-info">You can upload up to {{ Config::get('domain.advert.max_gallery_images') }}
			pictures to display on your listing.</p>

			<form action="{{ route('member.listings.gallery.store', [$advert->id]) }}"
				class="dropzone" id="gallery-dropzone">
				<input type="hidden" name="sequence" value="255" />
				<div class="dz-message">
				    Drag and drop files here, or click to upload. You can upload
				    multiple files at the same time!</span>
				</div>
			</form>

			<div class="gallery-finish-boxout">

				<h3>Finished uploading?</h3>

				<p>
					<a href="{{ route('member.listings.interstitial', ['id' => $advert->id]) }}"class="btn btn-primary">Yes, finished</a>
				</p>

			</div>

		</div>

		<div class="col-sm-6">

			<h2>Existing pictures</h2>

			<div class="alert alert-info" role="alert">
				<p>The <strong>first</strong> image listed will be used in search
				 listings. You can easily change the order of the photos here
				- just drag and drop</p>
			</div>

			@if ($advert->photos)

				<div id="gallery" class="sortable" 
					data-save-sort-url="{{ route('member.listings.gallery.save_sort', [$advert->id]) }}"
					data-fetch-url="{{ route('member.listings.gallery.fetch', [$advert->id, null]) }}">

					@foreach ($advert->photos as $photo)

						@include('member.listings._galleryitem', compact('advert', 'photo'))
					
					@endforeach

				</div>

			@endif

		</div>

	</div> {{-- /.Main row --}}

</div> {{-- /.panel-body --}}

@stop
