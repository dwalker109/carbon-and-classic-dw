@extends('member.layout')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h1>Recently Sold</h1>
				<a class="btn btn-default" href="{{ route('member.listings.create') }}" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New Advert</a>
			</div>

			<div class="panel-body">

				<h2>Recently Sold</h2>

				@if ($adverts->isEmpty())

					<div class="well">
						<p>You don't have any completed listings to display.</p>
					</div>

				@else

					@include('member.listings._table', compact('adverts'))

				@endif

			</div> {{-- /.panel-body --}}
		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop
