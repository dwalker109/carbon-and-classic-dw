<table class="table table-striped data-table" id="admin-adverts-index">

	<thead>
		<tr>
			<th class="table-widget-fixed-column" data-orderable="false">{{-- Controls --}}</th>
			<th>{{-- Title --}}</th>
			<th data-orderable="false">{{-- Approval status --}}</th>
			<th>Price</th>
			<th>Offers</th>
			<th>Sale Status</th>
		</tr>
	</thead>

	<tbody>
	@foreach ($adverts as $advert)
		<tr>
			<td>

				{{-- Manage dropdown --}}
				<div class="btn-group">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" aria-label="Manage Item">
						<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
					</button>
					<ul class="dropdown-menu" role="menu">

						{{-- Always show these options --}}
						<li>
							{{ Form::open(array('route' => array('member.listings.summary', $advert->id), 'method' => 'GET')) }}
							    <button type="submit" class="btn btn-link">All Offers</button>
							{{ Form::close() }}
						</li>

						{{-- Only show edit options if advert is not yet purchased --}}
						@if ($advert->purchase_status == Advert::ADVERT_AVAILABLE)
							<li>
								{{ Form::open(array('route' => array('member.listings.interstitial', $advert->id), 'method' => 'GET')) }}
								    <button type="submit" class="btn btn-link">Edit</button>
								{{ Form::close() }}
							</li>

							<li>
								{{ Form::open(array('route' => array('member.listings.destroy', $advert->id), 'method' => 'DELETE')) }}
								    <button type="submit" class="btn btn-link">Delete</button>
								{{ Form::close() }}
							</li>
						@endif

						{{-- Only show 'mark shipped' option is purchase is complete --}}
						@if ($advert->purchase_status == Advert::ADVERT_PURCHASED)
							<li>
								{{ Form::open(array('route' => array('member.listings.shipped', $advert->id), 'method' => 'POST')) }}
								    <button type="submit" class="btn btn-link">Mark as Shipped</button>
								{{ Form::close() }}
							</li>
						@endif

						{{-- Only show 'cancel sale' option if item is reserved but not paid for --}}
						@if ($advert->purchase_status == Advert::ADVERT_RESERVED)
							<li>
								{{ Form::open(array('route' => array('member.listings.cancel_sale', $advert->id), 'method' => 'POST')) }}
								    <button type="submit" class="btn btn-link">Cancel Offer and Re-list</button>
								{{ Form::close() }}
							</li>
						@endif											

					</ul>
				</div> {{--- ./Manage dropdown --}}

			</td>

			<td>
				@if ($advert->purchase_status == Advert::ADVERT_AVAILABLE && $advert->approval_status == Advert::ADVERT_APPROVED)
					{{ link_to_route('public.advert', $advert->title, ['slug' => $advert->slug], ['target' => '_blank']) }}
				@else 
					{{ link_to_route('public.advert.archived', $advert->title, ['slug' => $advert->slug], ['target' => '_blank']) }}
				@endif
			</td>

			<td>
				<?php $label_colour = $advert->approval_status == Advert::ADVERT_APPROVED ? 'primary' : 'warning' ?>
				<span class="label label-{{ $label_colour }}">{{ $advert->approval_status_string }}</span>
			</td>

			<td>{{ $advert->gbp_price_string }}</td>

			<td>
				@if (! $advert->offers()->pending()->get()->isEmpty())
					{{ link_to_route('member.listings.summary', 
						$advert->offers()->pending()->get()->count() . ' offers', 
						['id' => $advert->id]) 
					}}
				@endif
			</td>
			
			<td>
				@if ($advert->purchase_status == Advert::ADVERT_AVAILABLE)
					{{ $advert->purchase_status_string }}
				@else
					{{ link_to_route('member.listings.summary', $advert->purchase_status_string,
						['id' => $advert->id]) 
					}}
				@endif
			</td>
			
		</tr>
	@endforeach
	</tbody>
</table>