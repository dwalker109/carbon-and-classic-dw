@extends('member.layout')

@section('title', 'Listing complete')

@section('content')

<div class="row">

	<div class="col-xs-12 col-sm-6">

		<div class="jumbotron equal-height">

			<h2>{{ $advert->title }}</h2>
			@if ($advert->status == Advert::ADVERT_APPROVED)
				<p class="text-info">Your advert is live!</p>
			@else
				<p class="text-info">Your advert is currently awaiting approval</p>
			@endif

		</div>

	</div>

	<div class="col-xs-12 col-sm-6">

		<div class="jumbotron equal-height">

			<h2>What would you like to do next?</h2>
				{{ link_to_route('member.listings.interstitial', 'Make changes to this listing', [$advert->id], ['class' => 'btn btn-block btn-warning']) }}
				{{ link_to_route('member.listings.create', 'Add another listing', null, ['class' => 'btn btn-block btn-info']) }}
				{{ link_to_route('member.dashboard', 'Return to the dashboard', null, ['class' => 'btn btn-block btn-success']) }}
		</div>

	</div>

	<div class="text-center col-sm-12 hidden-xs">

		<span class="glyphicon glyphicon-ok text-success" style="font-size: 100px;"></span>

	</div>

	</div> {{-- /.jumbotron --}}
</div> {{-- /.row --}}

@stop
