<div class="row">

	<a href="{{ route('member.listings.summary', ['id' => $advert->id]) }}">
		<div class="col-xs-3">
			<img src="{{ $advert->primary_photo->image->url('thumb') }}" class="thumbnail">
		</div>
		<div class="col-xs-9">

			<p>{{ $advert->manufacturer->name }} {{ $advert->title }}, <b>{{ $advert->gbp_price_string }}</b>
			<?php $bs3_class = $advert->approval_status == Advert::ADVERT_APPROVED ? 'success' : 'warning' ?>
			<span class="label label-{{ $bs3_class }}">{{ $advert->approval_status_string }}</span></p>

			@if ($advert->winning_offer)
				
				<p>Sold for {{ $advert->winning_offer->gbp_offer_price_string }}

					@if($advert->purchase_status === Advert::ADVERT_RESERVED)
						<span class="label label-warning">{{ $advert->purchase_status_string }}</span>
					@elseif ($advert->purchase_status === Advert::ADVERT_PURCHASED || $advert->purchase_status === Advert::ADVERT_SHIPPED)
						<span class="label label-success">{{ $advert->purchase_status_string }}</span>
					@endif

				</p>

			@else

				<p>{{ $advert->offers()->count() }} offers</p>

			@endif

		</div>
	</a>

</div>
