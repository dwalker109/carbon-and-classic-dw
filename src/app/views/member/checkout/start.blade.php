@extends('public.layout')

@section('title', 'Checkout via PayPal')

@section('content')

{{ Form::open(['route' => ['offer.redeem.confirmed', $offer->acceptance_code]]) }}

<div class="row">

	<div class="col-sm-6">

		<h4>You will now be redirected to <strong>PayPal</strong> to complete
		your purchase of {{ $advert->title }}, at a cost of <b>{{ $offer->gbp_offer_price_string }}</b>
		plus delivery of:</h4>

		{{ Form::select(
			'postage_option',
			$advert->user_applicable_postage_options->lists('display_string'),
			null,
			['class' => 'form-control']
		) }}

		<h4>You will be asked to log in to PayPal, or pay with a credit/debit card
		directly - <strong>you are not required to create a PayPal account</strong>.</h4>


		<address class="alert alert-info">
		<h4>Address Details</h4>
			{{ $profile->full_name }}<br>
			{{ implode('<br>', $profile->address_components) }}
		</address>

	</div>

	<div class="col-sm-6">

		<div class="alert alert-warning">
			<p class="text-warning">Please make sure your address details are correct. 
			They have been used to calculate delivery charges and therefore will 
			be sent to PayPal as the confirmed address for the seller to deliver
			your purchase to.</p>
		</div>

		<div class="alert alert-warning">
			<p class="text-warning">If you require delivery to be made to 
			a different address, please {{ link_to_route('member.profile.manage', 'amend your profile') }}
			with the correct address details before completing this purchase.
			<strong>Please do not ask the seller to post to a different address after
			payment has been made.</strong></p>
		</div>

		<div class="alert alert-warning">
			<p class="text-warning">Please ensure that your contact telephone number
			is correct - the seller will probably need to contact you, especially if
			you have arranged to collect in person. If necessary, 
			{{ link_to_route('member.profile.manage', 'amend your profile') }}
			with your current phone number before completing this purchase.
			</p>
		</div>

	</div>

	<div class="col-xs-12">

		<div class="alert alert-info">

			<h4 class="text-center">All transactions are final. If you unable to complete 
			this transaction, please contact the seller immediately.</h4>

			{{ Form::submit('Proceed to PayPal payment', ['class' => 'btn btn-lg btn-primary center-block']) }}

		</div>


	</div>

</div>

{{ Form::close() }}

@stop
