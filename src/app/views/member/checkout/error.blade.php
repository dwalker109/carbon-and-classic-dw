@extends('public.layout')

@section('title', 'Checkout error')

@section('content')

<div class="row">

<div class="col-xs-12">

	<div class="jumbotron text-center">

		<h2>Sorry, an error occurred</h2>

		<p>Something has gone wrong and you cannot complete your purchase at this time.
		This can occur if:</p>

		<ul>
			<li>You recently cancelled an attempt to complete the sale</li>
			<li>There is a technical problem between us and PayPal</li>
		</ul>

		<p>Please wait a few minutes and try again. If the problem persists, 
		please contact us for assistance.</p>
		
	</div>

</div>

</div>

@stop
