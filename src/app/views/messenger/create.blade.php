@extends('messenger.layout')

@section('title', 'New message')

@section('content')

<h1>Create a new message</h1>

{{Form::open(['route' => 'messages.store'])}}

<div class="col-md-10 col-md-offset-1">
    <!-- Subject Form Input -->
    <div class="form-group">
        {{ Form::label('subject', 'Subject', ['class' => 'control-label']) }}
        {{ Form::text('subject', null, ['class' => 'form-control', 'required' => 'true']) }}
    </div>

    <!-- Message Form Input -->
    <div class="form-group">
        {{ Form::label('message', 'Message', ['class' => 'control-label']) }}
        {{ Form::textarea('message', null, ['class' => 'form-control textarea-simple']) }}
    </div>

    {{ Form::hidden('recipients[]', $recipient->id) }}
    
    <!-- Submit Form Input -->
    <div class="form-group">
        {{ Form::submit('Submit', ['class' => 'btn btn-primary form-control']) }}
    </div>
</div>

{{Form::close()}}

@stop
