{{-- Extend the member or admin layout as necessary --}}
@extends(Entrust::hasRole(Role::ROLE_ADMIN) ? 'admin.layout' : 'member.layout')
