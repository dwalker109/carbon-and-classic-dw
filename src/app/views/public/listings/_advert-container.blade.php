<div class="category-wrapper">

	{{-- Heading breadcrumbs --}}
	<section class="category-nav">

		<div class="category-nav-container">

			{{-- Hide filters button --}}
			<button id="toggle-filters-button" class="btn btn-xs btn-primary"
			title="Show/hide search filters"><span class="glyphicon glyphicon-transfer"> </span></button>
		
			{{-- Breadcrumbs, if exists --}}
			@unless ($category->getAncestorsAndSelf()->isEmpty())

				<ul class="breadcrumb">

					<li>{{ link_to_route('buying_home', 'Buy') }}</li>

					@foreach ($category->getAncestorsAndSelf() as $ancestor)

						<li>
							{{
								link_to_route(
									'public.category',
									$ancestor->name,
									$ancestor->slug
								)
							}}
						</li>

					@endforeach
					
				</ul>

			@endunless

		
		</div> {{-- ./category-nav-container --}}


	</section>

	<div id="category-inner-wrapper">

		{{-- Search filters pane --}}
		@include('public.listings._filters')

		{{-- Main pane --}}
		@include('public.listings._main')

	</div>{{-- ./#category-inner-wrapper --}}

</div>

