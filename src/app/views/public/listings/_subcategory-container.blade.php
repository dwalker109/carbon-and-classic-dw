<div class="category-wrapper">

	{{-- Heading breadcrumbs --}}
	<section class="category-nav">

		<div class="category-nav-container">

			{{-- Breadcrumbs, if exists --}}
			@unless ($category->getAncestorsAndSelf()->isEmpty())
				<ul class="breadcrumb">	

				<li>{{ link_to_route('buying_home', 'Buy') }}</li>

					@foreach ($category->getAncestorsAndSelf() as $ancestor)

						<li>
							{{
								link_to_route(
									'public.category',
									$ancestor->name,
									$ancestor->slug
								)
							}}
						</li>

					@endforeach
				</ul>
			@endunless

		
		</div> {{-- ./category-nav-container --}}

	</section>{{-- ./category-nav --}}

	{{-- Choose a view to render category content based on heirachy level --}}
	@include($category->isRoot() ? 'public.listings._top' : 'public.listings._main')

</div>