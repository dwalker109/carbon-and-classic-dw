@extends('public.layout')

@section('title', 'Search for bicycles and components')

@section('content')

<div class="search-wrapper">

	<section class="search-searchbox">

		{{-- Turn off bootstrap rendering and open a form--}}
		{{ Former::framework('Nude'); }}
		{{ Former::open() }}

		<div class="search-freetext">

				<div class="input-group">
				    {{ Former::text('freetext', '')->class('form-control')->placeholder('Search for...') }}
				    <span class="input-group-btn">
						<input type="submit" class="btn btn-default" value="Search">
				    </span> {{-- ./input-class-btn --}}
				</div> {{-- ./input-group --}}

		</div>

		<div class="search-type">

			{{ Former::select('search_type')->options([
				'listings' => 'Listings only', 'all' => 'All content'
			])->class('form-control') }}

		</div>


		{{ Former::close() }}

	</section>

	<section class="search-results-container">

		{{-- Placeholder text if no search submited --}}
		@unless ($search_terms)

			<div class="search-results-help">
				<h2>Enter terms to search for in the box above.</h2>
				<p>You can search for just advert listings, or opt to include site articles as well.</p>
			</div>

		@endunless

		{{-- Show results --}}
		@unless ( ! $adverts || $adverts->isEmpty())

			<div class="search-results-help">
				<h2>Found {{ $adverts->count() }} {{ Str::plural('Advert', $adverts->count()) }}</h2>
			</div>
		
			<div class="search-results">
				<div class="listing-list">
					@foreach ($adverts as $advert)

						@include('public.listings._advert-item')
					
					@endforeach
				</div>
			</div>

		@endunless

		@unless ( ! $cms_pages || $cms_pages->isEmpty())

			<div class="search-results-help">
				<h2>Found {{ $cms_pages->count() }} {{ Str::plural('Page', $cms_pages->count()) }}</h2>
			</div>

			<div class="search-results">
				<div class="listing-list">
					@foreach ($cms_pages as $page)

						<div class="listing-item">
							<div class="listing-item-container equal-height">

								<a href="{{ route('page', $page->slug) }}">
									<img src="{{ $page->primary_photo->image->url('medium') }}" />
									<div class="listing-item-name-and-location">
										<p>{{ $page->title }}</p>
									</div>
								</a>

							</div>
						</div>

					@endforeach
				</div>
			</div>

		@endunless

		{{-- Nothing found text --}}
		@if ($search_terms 
			&& (! $adverts || $adverts->isEmpty()) && (! $cms_pages || $cms_pages->isEmpty())
		)
			<div class="search-results-empty-image">
 				<img src="/images/uh-oh.jpg" alt="Uh oh!">
			</div>

			<div class="search-results-empty-text">
				<h2>Your search did not return any results</h2>
				<p>Try {{ link_to_route('buying_home', 'browsing') }} instead</p>
			</div>

		@endif

	</section>

</div>

@stop
