@extends('public.layout')

@section('title', "{$advert->title} (Archived)")

@section('content')

<div class="listing-wrapper">

	<section class="listing-nav">

		<div class="listing-nav-container">

			<ul class="breadcrumb">
				<li>Back to {{link_to_route(
						'public.category', 
						$advert->category->name,
						$advert->category->slug
					)}}
				</li>
			</ul>
		
		</div> {{-- ./listing-nav-container --}}

		<div class="listing-action-links">

			<ul class="breadcrumb">
				<li>This listing has been archived</li>
			</ul>

		</div> {{-- ./listing-action-links --}}

	</section>

	{{-- Include the main advert content --}}
	@include('public.advert._body_with_images')

</div>

@stop
