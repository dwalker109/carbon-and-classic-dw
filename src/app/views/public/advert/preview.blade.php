@extends('public.layout-popup')

@section('content')

<div class="well">
	<h1>You are viewing a preview of your listing</h1>
	<p>Once you are ready to submit it for approval, close this preview and
	choose the <b>Submit this listing</b> option.</p>
</div>

<?php $preview_mode = true ?>

<div class="listing-preview-container">

	<div class="listing-wrapper">

		{{-- Include the main advert content --}}
		@include('public.advert._body_with_images')

	</div>

</div>

@stop
