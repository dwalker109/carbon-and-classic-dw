@extends('public.layout')

@section('title', $advert->title)

@section('content')

<div class="listing-wrapper">

	<section class="listing-nav">

		<div class="listing-nav-container">

			<ul class="breadcrumb">
				<li>Back to {{link_to_route(
						'public.category', 
						$advert->category->name,
						$advert->category->slug
					)}}
				</li>
			</ul>
		
		</div> {{-- ./listing-nav-container --}}

		<div class="listing-action-links">


			{{-- If user isn't logged in, show a signup message --}}
			@if (! Confide::user())

				<ul class="breadcrumb">

					<li>To buy this item you will need to 
					{{ link_to_route('users.create', 'create a free account') }}</li>
					<li>Already a member?
					{{ link_to_route('users.login_and_return', 'Sign in now') }}</li>

				</ul>

			{{-- User logged in, so show the rest of the links --}}
			@else

				{{--  Show buy and offer links if delivery is possible --}}
				@if ($advert->is_deliverable_to_user)

					{{-- Buy --}}  {{-- link_to_route('checkout.start', 'Buy', $advert->slug) --}}
					<a class="show-popover btn btn-xs btn-success" role="button" 
					data-content='@include('public.advert._reserve-form')'>Buy</a>

					{{-- Offer form (single quotes on the data-content attrib are crucial) --}}
					<a class="show-popover btn btn-xs btn-warning" role="button" 
					data-content='@include('public.advert._offer-form')'>Make offer</a>

				@endif

				{{-- Message seller --}}
				{{ link_to_route('messages.create', "Message seller ({$advert->owner->username})", $advert->owner->username, 
					['class' => 'btn btn-xs btn-info']) 
				}}

			@endif

		</div> {{-- ./listing-action-links --}}

	</section>

	{{-- Include the main advert content --}}
	@include('public.advert._body_with_images')

</div>

@stop
