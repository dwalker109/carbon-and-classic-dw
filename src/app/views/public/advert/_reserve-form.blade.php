{{-- Partial used as a bootstrop popover - nake sure to only use double quotes ("")
in this file, single quotes will break the calling html element!!! --}}

<p>By puchasing this item you agree to complete a payment, via PayPal, within
three days. If you have questions about the item you should message the seller
before committing to buy.</p>

<div class="listing-reserve-confirm">
	{{ link_to_route('checkout.reserve', 'I want to purchase this item', 
	[$advert->slug], ['class' => 'btn btn-primary']) }}
</div>
