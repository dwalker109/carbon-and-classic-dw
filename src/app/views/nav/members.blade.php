<header class="navbar navbar-default cc-navbar-members">
	<div class="container-fluid">

		<nav class="collapse navbar-collapse bs-navbar-collapse">
			<ul class="nav navbar-nav navbar-right">

			<?php
				// Obtain counters so we can contexutalise them
				$overall_qty = Confide::user()->newMessagesCount() 
					+ Confide::user()->activities()->unread()->count();
				$messages_qty = Confide::user()->newMessagesCount();
				$activities_qty = Confide::user()->activities()->unread()->count();
			?>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						Alerts &amp; Messages 
						<span class="label label-as-badge label-{{ $overall_qty ? 'danger' : 'default' }}">{{ $overall_qty }}</span>	
					</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ URL::route('messages') }}">Messages <span class="label label-as-badge label-{{ $messages_qty ? 'danger' : 'default' }}">{{ $messages_qty }}</span></a></li>
						<li><a href="{{ URL::route('member.alerts.index') }}">Alerts <span class="label label-as-badge label-{{ $activities_qty ? 'danger' : 'default' }}">{{ $activities_qty }}</span></a></li>
					</ul>
					
				<li>{{ link_to_route('member.dashboard', 'Dashboard') }}</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Confide::user()->username }} </a>
					<ul class="dropdown-menu" role="menu">
					
						@if (Entrust::hasRole(Role::ROLE_ADMIN))
							<li>{{ link_to_route('admin.dashboard', 'Admin dashboard') }}</li>
							<li class="divider"></li>
						@endif

						<li>{{ link_to_route('member.profile.manage', 'My profile') }}</li>
						<li>{{ link_to_route('member.password', 'Change password') }}</li>
						<li><a href="{{ URL::route('logout') }}">Logout <span class="glyphicon glyphicon-log-out"></span></a></li>
					</ul>
				</li>

			</ul>
		</nav>

	</div>
</header>