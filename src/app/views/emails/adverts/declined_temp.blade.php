<h1>Sorry {{ $profile->full_name }}, your advert needs a few tweaks before it goes live</h1>

<h2>Summary</h2>
<p>Title: {{ $advert->title }}</p>
<p>Price: {{ $advert->gbp_price_string }}</p>

<p>Afer reviewing your advert we have decided that it requires some adjustment before
it can appear on the site.</p>

<p>We will contact you shortly with feedback on what you need to do in order to
complete your listing.</p>

<p>Thanks for your patience, and we look forward to getting your advert live soon!</p>
