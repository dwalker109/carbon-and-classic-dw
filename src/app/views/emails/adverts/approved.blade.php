<h1>Congratulations {{ $profile->full_name }}, your advert has been approved!</h1>

<h2>Summary</h2>
<p>Title: {{ $advert->title }}</p>
<p>Price: {{ $advert->gbp_price_string }}</p>

<p>The advert is now {{ link_to_route('public.advert', 'visible on the site', [$advert->slug]) }}
and eligeable for purchase. Please make sure you respond to any queries from potential purchasers 
in a timely manner.</p>

<p>If you need to {{ link_to_route('member.listings.edit', 'make changes to your advert', [$advert->id]) }},
please note that it will be temporarily unavailable while it is approved once again.</p>
