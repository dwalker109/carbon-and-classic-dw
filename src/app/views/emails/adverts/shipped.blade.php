<h1>Congratulations, your purchase has been shipped!</h1>

<h2>Summary</h2>
<p>Title: {{ $advert->title }}</p>
<p>Price: {{ $advert->winning_transaction->gbp_purchase_price_string }}</p>

<ul>
	<li>If you have any issues, please contact the seller in the first instance.</li>
	<li>If the seller is not able to resolve your issues, please get in touch with us directly.</li>
</ul>

<p>Thanks for using Carbon and Classic!</p>