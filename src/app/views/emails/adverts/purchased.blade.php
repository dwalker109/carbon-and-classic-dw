<h1>Congratulations, your purchase has been completed!</h1>

<h2>Summary</h2>
<p>Title: {{ $advert->title }}</p>
<p>Price: {{ $advert->winning_transaction->gbp_purchase_price_string }}</p>

<p>You will be contacted by the seller shortly to arrange or confirm delivery.</p>
<p>You may also contact them directly using the details below:</p>

<h2>Seller's contact details:</h2>
<p>
	{{ $advert->owner->profile->full_name }}<br>
	{{ implode('<br>', $advert->owner->profile->address_components) }}
</p>

<ul>
	<li>If you have any issues, please contact the seller in the first instance.</li>
	<li>If the seller is not able to resolve your issues, please get in touch with us directly.</li>
</ul>

<p>Thanks for using Carbon and Classic!</p>