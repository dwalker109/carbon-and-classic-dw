<h1>An offer has been made on one of your adverts</h1>

<h2>Summary</h2>
<p>Title: {{ $advert->title }}</p>
<p>Price: {{ $advert->gbp_price_string }}</p>
<p>Minimum offer price: {{ $advert->gbp_min_offer_price_string }}
<p>Offer: {{ $offer->gbp_offer_price_string }}</p>

<p>If the offer is below your minimum offer price, it will have been declined 
automatically. If it is above you minimum offer price, you should login to you
{{ link_to_route('member.listings.index', 'offers dashboard') }} and
action it.</p>