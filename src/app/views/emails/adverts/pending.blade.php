<h1>An advert is awaiting approval!</h1>

<h2>Summary</h2>
<p>Title: {{ $advert->title }}</p>
<p>Price: {{ $advert->gbp_price_string }}</p>

<p>You should now {{ link_to_route('admin.adverts.edit', 'view the listing', [$advert->id]) }}
and take action.</p>

<p>The member will receive an email once you do this, telling them the outcome of the approval.</p>

<p>If you decline the advert, you should follow up with a message telling them
what they need to change, or why the advert is not suitable.</p>
