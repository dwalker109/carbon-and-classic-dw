{{ Former::populate($advert) }}
{{ Former::withRules($rules) }}

{{ Former::select('approval_status')->options($approval_statuses) }}

{{ Former::text('title') }}
{{ Former::textarea('body')->rows(3) }}

{{ Former::text('price')->disabled() }}
{{ Former::text('min_offer_price', 'Reserve price')->disabled() }}

{{ Former::select('category_id', 'Category')->options($categories)->placeholder('Please select')->addClass('hierarchy not-select2') }}
{{ Former::select('manufacturer_id', 'Manufacturer')->options($manufacturers)->placeholder('Please select') }}

<span id="mfr_other_freetext_container">
	{{ Former::text('mfr_other_freetext', ' ')->placeholder('Enter manufacturer here') }}
</span>


{{ Former::actions(
	Button::primary('Save')->submit(),
	Button::danger('Cancel')->asLinkTo(route('admin.adverts.index'))
) }}
