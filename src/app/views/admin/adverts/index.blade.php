@extends('admin.layout')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">

		<h1>Adverts</h1>

		<div class="panel panel-primary">

			<div class="panel-body">

				@if ($adverts->isEmpty())

					<div class="alert alert-warning" role="alert">
						<p>No adverts found</p>
					</div>

				@else

					<table class="table table-striped data-table" id="admin-adverts-index">

						<thead>
							<tr>
								<th data-orderable="false">{{-- Controls --}}</th>
								<th>{{-- Title --}}</th>
								<th>Seller</th>
								<th>Added</th>
								<th>Approval Status</th>
								<th>Sale Status</th>

							</tr>
						</thead>

						<tbody>
						@foreach ($adverts as $advert)
							<tr>
							
								<td>

									{{-- Manage dropdown --}}
									<div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" aria-label="Manage Item">
											<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
										</button>
										<ul class="dropdown-menu" role="menu">
											<li>
												{{ Form::open(array('route' => array('admin.adverts.edit', $advert->id), 'method' => 'GET')) }}
												    <button type="submit" class="btn btn-link">Edit</button>
												{{ Form::close() }}
											</li>
											<li>
												{{ Form::open(array('route' => array('messages.create', $advert->owner->username), 'method' => 'GET')) }}
												    <button type="submit" class="btn btn-link">Message Seller</button>
												{{ Form::close() }}
											</li>
											<li>
												{{ Form::open(array('route' => array('admin.adverts.destroy', $advert->id), 'method' => 'DELETE')) }}
												    <button type="submit" class="btn btn-link">Delete</button>
												{{ Form::close() }}
											</li>
										</ul>
									</div> {{--- ./Manage dropdown --}}

								</td>

								<td><strong>{{ $advert->title }}</strong></td>
								<td>{{ $advert->owner->username }}</td>
								<td>{{ $advert->created_at }}</td>
								<td>{{ $advert->approval_status_string }}</td>
								<td>{{ $advert->purchase_status_string }}</td>

							</tr>
						@endforeach
						</tbody>
					</table>

				@endif

			</div> {{-- /.panel-body --}}
		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop
