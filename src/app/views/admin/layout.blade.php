<!DOCTYPE html>
<html lang="en-GB">
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Admin Dashboard :: Carbon &amp; Classic</title>

		{{-- Load CSS --}}
		@foreach(array_merge($shared_css, [
	        '/styles/compiled/admin.css',
        ]) as $css_file)
			{{ HTML::style($css_file) }}
		@endforeach

		{{-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --}}
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body>

		{{-- Bootstrap navbar --}}
		@include('admin.navbar')

		{{-- Messages container --}}
		<div id="messages-popup">
			{{ Notification::showAll() }}
		</div>

		{{-- Output main content in a BS3 container --}}
		<div class="container">
			@yield('content')
		</div> {{-- ./container --}}

		{{-- Load Javascript --}}
		@foreach(array_merge($shared_js, [
			'/javascripts/admin-bindings.js',
        ]) as $js_file)
			{{ HTML::script($js_file) }}
		@endforeach

	</body>
</html>
