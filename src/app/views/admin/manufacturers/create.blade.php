@extends('admin.manufacturers._form')

@section('main')

<div class="panel-heading">
	<h1>Add Manufacturer</h1>
</div>

<div class="panel-body">

	{{ Former::open(route('admin.manufacturers.store'))->method('POST') }}

		@include('admin.manufacturers._fields')

	{{ Former::close() }}

</div> {{-- /.panel-body --}}

@stop