@extends('admin.layout')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">

		<div class="panel panel-primary">

		@yield('main')

		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop