@extends('admin.cms_pages._form')

@section('main')

<div class="panel-heading">
	<h1>Edit CMS Page</h1>
</div>

<div class="panel-body">

	{{ Former::open(route('admin.cms-pages.update', $cms_page->id))->method('PUT') }}

		@include('admin.cms_pages._fields')

	{{ Former::close() }}

</div> {{-- /.panel-body --}}

@stop