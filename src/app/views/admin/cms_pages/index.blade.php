@extends('admin.layout')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">
		
		<h1>CMS Pages</h1>

		<div class="alert alert-info" role="alert">
			<p>Pages can be <strong>nested</strong> underneath others to create
			a heirachy. A page can also be <strong>hidden</strong> from navigation
			so it can be linked to, but will not appear in the page lists. 
			You can also add an <strong>alias</strong>, which allows you to create
			a link in the navigation menu to a non-cms page on the site, or 
			even an external url.</p>
			<p class="text-danger">Note that if page headings and lists have been 
			manually placed, these options won't have any effect (for example,
			the homepage navbar and page footer items are manually placed).</p>
		</div>

		<div class="panel panel-primary">

			<div class="panel-heading">
				<a class="btn btn-default" href="{{ route('admin.cms-pages.create') }}" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New CMS Page</a>
			</div>

			<div class="panel-body">

				@if ($cms_pages->isEmpty())

					<div class="alert alert-warning" role="alert">
						<p>No pages found</p>
					</div>

				@else 

					<table class="table table-striped data-table" id="admin-pages-index">
						<thead>
							<tr>

								<th data-orderable="false"></th>
								<th></th>
								<th></th>
								<th></th>

							</tr>
						</thead>
						<tbody>
						@foreach ($cms_pages as $cms_page)
							<tr>
								<td>

									{{-- Manage dropdown --}}
									<div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" aria-label="Manage Item">
											<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
										</button>
										<ul class="dropdown-menu" role="menu">
											<li>
												{{ Form::open(array('route' => array('admin.cms-pages.edit', $cms_page->id), 'method' => 'GET')) }}
												    <button type="submit" class="btn btn-link">Edit</button>
												{{ Form::close() }}
											</li>
											<li>
												{{ Form::open(array('route' => array('admin.cms-pages.destroy', $cms_page->id), 'method' => 'DELETE')) }}
												    <button type="submit" class="btn btn-link">Delete</button>
												{{ Form::close() }}
											</li>
										</ul>
									</div> {{--- ./Manage dropdown --}}

								</td>

								{{-- Output category ancestor chain and own title --}}
								<td>
									@foreach ($cms_page->getAncestors() as $ancestor)
										{{ $ancestor->title }} &rArr;
									@endforeach
									<strong>{{ $cms_page->title }}</strong>
								</td>
								
								<td>{{ $cms_page->active ? 'Active' : 'Inactive' }}</td>
								<td>{{ $cms_page->nav_hidden ? 'Hidden' : 'Visible' }}</td>


							</tr>
						@endforeach
						</tbody>
					</table>

				@endif

			</div> {{-- /.panel-body --}}
		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop