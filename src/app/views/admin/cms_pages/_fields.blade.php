{{ Former::populate($cms_page) }}
{{ Former::withRules($rules) }}

{{ Former::select('parent_id', 'Parent Page')->options(['N/A'] + $cms_pages) }}
{{ Former::text('title') }}
{{ Former::textarea('body')->rows(3) }}
{{ Former::checkbox('active')->check() }}

{{-- Former::checkbox('nav_hidden') --}}

{{ Former::text('redirect_url') }}
{{ Former::text('meta_description') }}

{{ Former::actions(
	Button::primary('Save')->submit(),
	Button::danger('Cancel')->asLinkTo(route('admin.cms-pages.index'))
) }}
