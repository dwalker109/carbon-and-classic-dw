<?php

class HomeController extends BaseController
{

    /**
     * Show the homepage
     * 
     * @return Response
     */
    public function index()
    {
        return View::make('public.home');
    }


    /**
     * Show a CMS page
     *
     * @param CmsPage
     * @return Response
     */
    public function page(CmsPage $cms_page)
    {
        return View::make('public/cms-page', compact('cms_page'));
    }
}
