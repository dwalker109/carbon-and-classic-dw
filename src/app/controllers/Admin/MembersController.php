<?php

namespace Admin;

// Domain
use User;
use Role;

// Core
use Input;
use Redirect;
use View;

// Third Party
use Notification;

class MembersController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();
        return View::make('admin.members.index', compact('users'));
    }


    /**
     * Make admin
     *
     * @param  User   $user
     * @return Response
     */
    public function promote(User $user)
    {
        $user->setBaseRole(Role::ROLE_ADMIN);
        Notification::success('Role changed');
        return Redirect::route('admin.members.index');
    }


    /**
     * Make standard member
     *
     * @param  User   $user
     * @return Response
     */
    public function demote(User $user)
    {
        $user->setBaseRole(Role::ROLE_MEMBER);
        Notification::success('Role changed');
        return Redirect::route('admin.members.index');
    }


    /**
     * Deactivate (remove all base roles)
     *
     * @param  User   $user
     * @return Response
     */
    public function deactivate(User $user)
    {
        $user->deactivate();
        Notification::success('User deactivated');
        return Redirect::route('admin.members.index');
    }
}
