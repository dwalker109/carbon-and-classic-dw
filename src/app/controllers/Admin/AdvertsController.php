<?php

namespace Admin;

// Domain
use Advert;
use Category;
use Manufacturer;

// Core
use Input;
use Redirect;
use View;

// Third Party
use Notification;

class AdvertsController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $adverts = Advert::orderBy('updated_at', 'desc')->get();
        return View::make('admin.adverts.index', compact('adverts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Advert $advert
     * @return Response
     */
    public function edit(Advert $advert)
    {
        $approval_statuses = Advert::$approval_statuses;
        $rules = $advert->getRules();
        $categories = Category::getFullHierachyNestedList();
        $manufacturers = Manufacturer::lists('name', 'id');

        return View::make(
            'admin.adverts.edit',
            compact(
                'advert',
                'approval_statuses',
                'rules',
                'categories',
                'manufacturers'
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Advert $advert
     * @return Response
     */
    public function update(Advert $advert)
    {
        if ($advert->fill(Input::except(['price', 'min_offer_price']))->save()) {
            Notification::success('Advert saved');
            return Redirect::route('admin.adverts.index');
        } else {
            return Redirect::back()->withErrors($advert->getErrors())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Advert $advert
     * @return Response
     */
    public function destroy(Advert $advert)
    {
        $advert->delete();
        Notification::success('Advert deleted');
        return Redirect::route('admin.adverts.index');
    }


    /**
     * Display a cut down preview version of the frontend page
     * 
     * @param  Advert $advert
     * @return Response
     */
    public function preview(Advert $advert)
    {
        return View::make('admin.adverts.preview', compact('advert'));
    }
}
