<?php

// Core
use Input;
use Redirect;
use View;

// Domain
use Advert;
use Category;
use CmsPage;

class ListingsController extends \BaseController
{

    /**
     * Show the buying homepage
     * 
     * @return Response
     */
    public function index()
    {
        return View::make('public.listings.home');
    }


    /**
     * Show a category - whole page, including filters menu and content or,
     * for AJAX, just the content pane
     *
     * @param  Category $category
     * @return Response
     */
    public function category($category)
    {
        if (Request::ajax()) {
            // Early return the content partial only
            SearchFilters::prepareInput();
            return View::make('public.listings._main-content', compact('category'));
        }

        // Still here - build the filters and return the whole page
        $search_filters = SearchFilters::build($category);
        return View::make('public.listings.body', compact('category', 'search_filters'));
    }


    /**
     * Reset (clear) the submitted searchfilters session data and return to previous page
     */
    public function reset()
    {
        SearchFilters::resetInput();
        return Redirect::back();
    }


    /**
     * Show an advert
     *
     * @param  Advert $advert
     * @return Response
     */
    public function advert($advert)
    {
        $rules = Offer::$static_rules;
        return View::make('public.advert.normal', compact('advert', 'search_filters', 'rules'));
    }


    /**
     * Show a read only advert (no offers, purchase etc)
     *
     * @param  Advert $advert
     * @return Response
     */
    public function archivedAdvert($advert)
    {
        return View::make('public.advert.archived', compact('advert'));
    }
}
