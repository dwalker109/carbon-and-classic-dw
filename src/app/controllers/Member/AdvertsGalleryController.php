<?php

namespace Member;

// Domain
use Advert;
use GalleryPicture;
use Confide;

// Core
use Input;
use Redirect;
use View;
use Response;
use URL;
use Session;
use Config;
use \Illuminate\Database\Eloquent\Collection;

// Third Party
use Notification;

// Note that an AdvertGallery doesn't have its own model - the gallery items
// are retrived directly into the Adverts model via a polymorphic relationship

class AdvertsGalleryController extends \BaseController
{

    /**
     * Register filters
     */
    public function __construct()
    {
        $this->beforeFilter('extended_profile_holder', ['except' => 'index', 'delete']);
    }


    /**
     * Display all contained gallery items
     *
     * @param  Advert  $advert
     * @return Response
     */
    public function index(Advert $advert)
    {
        return View::make('member.listings.gallery', compact('advert'));
    }


    /**
     * Retrieve HTML for a specific item
     *
     * @param  Advert $advert
     * @param  GalleryPicture $photo
     * @return Response
     */
    public function fetch(Advert $advert, GalleryPicture $photo)
    {
        return View::make('member.listings._galleryitem', compact('advert', 'photo'));
    }


    /**
     * Store a new gallery item and set advert to draft- Ajax endpoint
     *
     * @param  Advert  $advert
     * @return Response
     */
    public function store(Advert $advert)
    {
        // Make a new model from input
        $photo = new GalleryPicture(Input::all());

        // Early return if too many images already
        $max_gallery_images = Config::get('domain.advert.max_gallery_images');
        if ($advert->photos->count() >= $max_gallery_images) {
            return Response::json("Gallery is limited to {$max_gallery_images} images", 403);
        }

        // Still here, save
        if ($advert->photos()->save($photo)) {
            $advert->makeDraft();
            return Response::json($photo, 200);
        } else {
            return Response::json('Upload failed', 400);
        }
    }


    /**
     * Update sorting info for the gallery - no draft status for this
     *
     * @param  Advert $advert
     * @return Response
     */
    public function sort(Advert $advert)
    {
        foreach (Input::get('id_list') as $sequence => $id) {
            // For each posted id, go via photos() relationship to silently skip
            // any attempt to manipulate images not assigned to this advert
            if ($photo = $advert->photos()->where('id', '=', $id)->first()) {
                $photo->fill(['sequence' => $sequence])->save();
            }
            
        }

        return Response::json('Saved', 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Advert  $advert
     * @return Response
     */
    public function update(Advert $advert)
    {
        // Update image meta, order etc.
        return Response::json('Not implemented', 400);
    }


    /**
     * Remove the specified resource from storage - draft status not required.
     *
     * @param  Advert  $advert
     * @param  GalleryPicture $photo
     * @return Response
     */
    public function destroy(Advert $advert, GalleryPicture $photo)
    {
        $photo->delete();
        return Response::json('Deleted', 200);
    }
}
