<?php

namespace Member;

use Profile;

use Confide;
use Notification;

use Input;
use Redirect;
use View;
use URL;

class ProfileController extends \BaseController
{

    /**
     * Show profile edit form
     *
     * @return  Profile
     */
    public function manage()
    {
        $user = Confide::user();
        $profile = Profile::firstOrNew(['user_id' => $user->id]);
        $rules = $profile->getRules();
        return View::make('member.manage-profile', compact('profile', 'rules'));
    }


    /**
     * Process an edit form submission
     */
    public function doManage()
    {
        $user = Confide::user();
        $profile = Profile::firstOrNew(['user_id' => $user->id]);
        if ($profile->fill(Input::except('user_id'))->save()) {
            Notification::success('Your profile has been saved');
            return Redirect::intended(URL::route('member.dashboard'));
        } else {
            return Redirect::back()->withErrors($profile->getErrors())->withInput();
        }
    }


    /**
     * Show change password form
     */
    public function password()
    {
        return View::make('member.update-password');
    }


    /**
     * Process change password form submission
     */
    public function doPassword()
    {
        $repo = \App::make('UserRepository');
        if ($repo->changePassword(Confide::user(), Input::all()) === true) {
            Notification::success('Your password has been changed');
            return Redirect::route('member.dashboard');
        } else {
            Notification::error('Could not update your password, please try again');
            return Redirect::back();
        }
    }
}
