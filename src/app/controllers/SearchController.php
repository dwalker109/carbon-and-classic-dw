<?php

// Core
use Input;
use Redirect;
use View;

// Domain
use Advert;
use Category;
use CmsPage;

class SearchController extends \BaseController
{

    /**
     * Do a standard site search with results page
     */
    public function siteSearch()
    {
        $adverts = null;
        $cms_pages = null;
        $search_terms = null;

        if (Input::has('freetext')) {

            $search_terms = Input::get('freetext');
            $adverts = Advert::search(Input::get('freetext'))->with('manufacturer')
                ->frontendVisible()->get();

            if (Input::get('search_type') == 'all') {
                $cms_pages = CmsPage::search(Input::get('freetext'))->frontendVisible()->get();;
            }
        }

        return View::make('public.search', compact('adverts', 'cms_pages', 'search_terms'));
    }
}
