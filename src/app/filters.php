<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
    //
});


App::after(function($request, $response)
{
    //
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
    if (Auth::guest())
    {
        if (Request::ajax())
        {
            return Response::make('Unauthorized', 401);
        }
        return Redirect::guest('login');
    }
});


Route::filter('auth.basic', function()
{
    return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
    if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
    if (Session::token() !== Input::get('_token'))
    {
        throw new Illuminate\Session\TokenMismatchException;
    }
});


/*
|--------------------------------------------------------------------------
| Entrust Filters
|--------------------------------------------------------------------------
|
| Protect access to resources based on roles and permissions - used instead of
| the standard Laravel auth filters
|
*/

Route::filter('entrust_admin', function() {
    if (! Entrust::hasRole(Role::ROLE_ADMIN)) {
        return Redirect::guest(URL::route('login'));
    }
});

Route::filter('entrust_member', function() {
    if (! Entrust::hasRole(Role::ROLE_MEMBER)) {
        return Redirect::guest(URL::route('login'));
    }
});

Route::filter('entrust_user', function() {
    if (! Entrust::hasRole(Role::ROLE_MEMBER) && ! Entrust::hasRole(Role::ROLE_ADMIN)) {
        return Redirect::guest(URL::route('login'));
    }
});


/*
|--------------------------------------------------------------------------
| Member Listing Edits
|--------------------------------------------------------------------------
|
| Maintiain DB integrity: stop add/edits if user profiles are missing info
|
*/

Route::filter('profile_holder', function() {
    if (! Confide::user()->is_profile_holder) {
        //
        $link = link_to_route(
            'member.profile.manage',
            'complete your user profile',
            null,
            ['class' => 'alert-link']
        );

        // Return forbidden for AJAX requests, redirect for normal requests
        if (Request::ajax()) {
            return Response::make('Not permitted', 403);
        } else {
            Notification::info("You will need to {$link} before you can do that!");
            return Redirect::guest(URL::route('member.profile.manage'));
        }
    }
});

Route::filter('extended_profile_holder', function() {
    if (! Confide::user()->is_extended_profile_holder) {
        //
        $link = link_to_route(
            'member.profile.manage',
            'add a PayPal ID to your user profile',
            null,
            ['class' => 'alert-link']
        );

        // Return forbidden for AJAX requests, redirect for normal requests
        if (Request::ajax()) {
            return Response::make('Not permitted', 403);
        } else {
            Notification::info("You will need to {$link} before you can do that!");
            return Redirect::guest(URL::route('member.profile.manage'));
        }
    }
});
