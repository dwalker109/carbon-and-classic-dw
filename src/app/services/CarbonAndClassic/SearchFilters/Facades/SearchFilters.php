<?php

namespace CarbonAndClassic\SearchFilters\Facades;

use Illuminate\Support\Facades\Facade;

class SearchFilters extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'searchfilters';
    }
}
