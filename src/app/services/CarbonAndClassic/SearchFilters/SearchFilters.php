<?php

namespace CarbonAndClassic\SearchFilters;

// Laravel core
use Cache;
use Collection;
use Input;
use Session;

// Domain
use Manufacturer;
use Category;
use Advert;

/**
 * Utility class for handling search filters, which need to be
 * pruned based on the current category
 */

class SearchFilters
{


    /**
     * Persist form input in the session so it survives page navigation
     */
    public function prepareInput()
    {
        // Perist/setup the posted form in Session
        if (Input::all()) {
            Session::set('searchform_data', Input::all());
        } elseif (! Session::has('searchform_data')) {
            Session::set('searchform_data', array());
        }
    }


    /**
     * Reset the search to a vanilla state
     */
    public function resetInput()
    {
        Session::set('searchform_data', array());
    }


    /**
     * Get all filters relevant to the current category
     *
     * @return Collection
     */
    public function build($category)
    {
        // Ensure input has been added to session
        $this->prepareInput();

        // Setup search_filters container, which will be returned at the end
        // with all the elements needed for the filters sidebar (sort)
        $search_filters = (object) [
            // Roundtrip data (as a collection)
            'form_populate_data' => Collection::make(Session::get('searchform_data')),
            // Sorting controls (as a collection)
            'advert_sort' => Collection::make(Advert::$sort)->lists('label'),
            // Manufacturer and price filters will be filled in shortly
            'manufacturers' => [],
            'advert_prices' => null,
        ];


        // Get manufacturers filtered by the current category
        Manufacturer::whereHas('adverts', function($aq) use ($category) {

            $aq->frontendVisible();
            $aq->whereHas('category', function($cq) use ($category) {

                // Nested set query is taken from the Baum Node descendantsAndSelf()
                // scope and reproduced here - I can't get it to merge with the
                // $cq query for some reason...
                $cq->where($category->getLeftColumnName(), '>=', $category->getLeft());
                $cq->where($category->getLeftColumnName(), '<', $category->getRight());

            });

        })->get()->each(function($category) use (&$search_filters) {

            foreach ($category->adverts as $advert) {

                // Add each valid manufacturer in a suitable format for Former
                $search_filters->manufacturers[$advert->manufacturer->name] = [
                    'id' => Manufacturer::SFKEY . "{$advert->manufacturer->id}",
                    'name' => Manufacturer::SFKEY . "[{$advert->manufacturer->id}]",
                    'value' => $advert->manufacturer->id,
                ];

            }

        }); // End get manufacturers

        // Get prices filtered by the current category
        $search_filters->advert_prices = (object) [

            'min' => floor(Advert::whereHas('category', function($cq) use ($category) {
                $cq->where($category->getLeftColumnName(), '>=', $category->getLeft());
                $cq->where($category->getLeftColumnName(), '<', $category->getRight());
            })->frontendVisible()->min('adverts.price')),

            'max' => floor(Advert::whereHas('category', function($cq) use ($category) {
                $cq->where($category->getLeftColumnName(), '>=', $category->getLeft());
                $cq->where($category->getLeftColumnName(), '<', $category->getRight());
            })->frontendVisible()->max('adverts.price')),

            'slider_field_name' => Advert::SFKEY_PRICE,
        ]; // End price

        return $search_filters;
    }
}
