<?php

namespace CarbonAndClassic\AdaptivePayments\Facades;

use Illuminate\Support\Facades\Facade;

class AdaptivePayments extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'adaptivepayments';
    }
}
