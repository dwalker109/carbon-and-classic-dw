<?php

namespace CarbonAndClassic\ActivitiesFeed;

use Illuminate\Support\ServiceProvider;

class ActivitiesFeedServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'activitiesfeed' instance container to our ActivitiesFeed object
        $this->app['activitiesfeed'] = $this->app->share(function($app) {
            return new ActivitiesFeed;
        });

        // Shortcut so developers don't need to add an Alias in app/config/app.php
        $this->app->booting(function() {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias(
                'ActivitiesFeed',
                'CarbonAndClassic\ActivitiesFeed\Facades\ActivitiesFeed'
            );
        });
    }
}
