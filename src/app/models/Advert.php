<?php

// Core
use Carbon\Carbon;
use Illuminate\Support\Collection;
// Composer
use Watson\Validating\ValidatingTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

class Advert extends Eloquent implements SluggableInterface
{
    // Consts to use when building searchfilters
    const SFKEY_PRICE = 'searchfilter_advert_prices_';
    const SFKEY_SORT = 'searchfilter_advert_sort';

    use SoftDeletingTrait;

    use ValidatingTrait;

    use SluggableTrait;

    use SearchableTrait;

    protected $fillable = [
        'approval_status', 'title', 'body', 'price', 'min_offer_price',
        'category_id', 'manufacturer_id', 'mfr_other_freetext',
        'purchased_at', 'reserved_at', 'shipped_at',
    ];

    protected $dates = ['purchased_at', 'reserved_at', 'shipped_at'];

    protected $sluggable = [
        'build_from' => 'title',
    ];

    protected $searchable = [
        'columns' => [
            'title' => 10,
            'body' => 5,
            'manufacturers.name' => 8,
            'mfr_other_freetext' => 8,
        ],
        'joins' => [
            'manufacturers' => ['adverts.manufacturer_id', 'manufacturers.id'],
        ],
    ];

    protected $rules; // Rules will be set in the constructor in this model

    protected $cachedDirty = []; // Used to cache the result of isDirty()

    // Def class constants and a container to reference the approval status
    const ADVERT_PENDING = 0;
    const ADVERT_DECLINED_TEMP = 1;
    const ADVERT_DECLINED_PERM = 2;
    const ADVERT_APPROVED = 3;
    const ADVERT_DRAFT = 4;
    public static $approval_statuses = [
        self::ADVERT_DRAFT => 'Draft',
        self::ADVERT_PENDING => 'Pending',
        self::ADVERT_DECLINED_TEMP => 'Declined - needs to be amended',
        self::ADVERT_DECLINED_PERM => 'Declined - rejected, needs deletion',
        self::ADVERT_APPROVED => 'Approved',
    ];

    // Def class constants and a container to reference the purchase status
    const ADVERT_PURCHASED = 0;
    const ADVERT_AVAILABLE = 1;
    const ADVERT_RESERVED = 2;
    const ADVERT_SHIPPED = 3;
    public static $purchase_statuses = [
        self::ADVERT_PURCHASED => 'Purchased',
        self::ADVERT_AVAILABLE => 'Available',
        self::ADVERT_RESERVED => 'Reserved',
        self::ADVERT_SHIPPED => 'Shipped',
    ];

    // Def sort labels
    public static $sort = [
        ['label' => 'Date added, oldest first', 'col' => 'created_at', 'dir' => 'asc'],
        ['label' => 'Data added, newest first', 'col' => 'created_at', 'dir' => 'desc'],
        ['label' => 'Price, lowest first', 'col' => 'price', 'dir' => 'asc'],
        ['label' => 'Price, highest first', 'col' => 'price', 'dir' => 'desc'],
        ['label' => 'Title', 'col' => 'title', 'dir' => 'asc'],
    ];

    ///////////////
    // Bootstrap //
    ///////////////

    /**
     * Set this model's rules on instantiation (requires logic and external values).
     */
    public function __construct()
    {
        parent::__construct();

        // Get config and processed values to use in rules
        $min_price = Config::get('domain.advert.min_price', 1);
        $max_price = Config::get('domain.advert.max_price', 99999.99);
        $min_offer_price_percentage = Config::get('domain.advert.min_offer_price_percentage', 50);
        $valid_approval_statuses_string = implode(',', array_keys(self::$approval_statuses));
        $advert_container_categories_string = implode(
            ',',
            Category::isAdvertContainer()->get()->lists('id')
        );

        $this->rules = [
            'approval_status' => ["in:{$valid_approval_statuses_string}"],
            'title' => ['required', 'max:255'],
            'body' => ['required', 'max:16383'],
            'active' => ['integer'],
            'price' => ['required', 'numeric', "between:{$min_price},{$max_price}"],
            'min_offer_price' => [
                'sometimes',
                'numeric',
                "is_percentage_of:price,{$min_offer_price_percentage},100",
            ],
            'category_id' => ['required', "in:{$advert_container_categories_string}"],
            'manufacturer_id' => ['required', 'exists:manufacturers,id'],
            'mfr_other_freetext' => ['max:255'],
        ];
    }

    /**
     * Boot the parent, register observer.
     */
    public static function boot()
    {
        parent::boot();

        self::observe(new AdvertObserver());
    }

    ///////////////////
    // Relationships //
    ///////////////////

    /**
     * Define belongsTo relationship with owner user.
     *
     * @return User
     */
    public function owner()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Define belongsTo relations with containing category.
     *
     * @return Category
     */
    public function category()
    {
        return $this->belongsTo('Category')->withTrashed();
    }

    /**
     * Define belongsTo relationship with manufacturer.
     *
     * @return Manufacturer
     */
    public function manufacturer()
    {
        return $this->belongsTo('Manufacturer')->withTrashed();
    }

    /**
     * Define hasMany relationship with transactions.
     *
     * @return Collection
     */
    public function transactions()
    {
        return $this->hasMany('Transaction');
    }

    /**
     * Define hasMany relationship with offers.
     *
     * @return Collection
     */
    public function offers()
    {
        return $this->hasMany('Offer')->orderBy('id', 'desc');
    }

    /**
     * Define polymorphic relationship with gallery photos (always sort by sequence).
     *
     * @return Collection
     */
    public function photos()
    {
        return $this->morphMany('GalleryPicture', 'imageable')->orderBy('sequence');
    }

    /**
     * Define hasmany relationship with postage options.
     *
     * @return Collection
     */
    public function postage()
    {
        return $this->hasMany('PostageOption')->orderBy('region_name');
    }

    ///////////////
    // Accessors //
    ///////////////

    /**
     * Ensure approval status is returned as an int (PHP MySQL driver issues).
     *
     * @return int
     */
    public function getApprovalStatusAttribute()
    {
        return (integer) $this->attributes['approval_status'];
    }

    /**
     * Virtual property for display of advert approval status.
     *
     * @return string
     */
    public function getApprovalStatusStringAttribute()
    {
        return self::$approval_statuses[$this->approval_status];
    }

    /**
     * Virtual property for calculating advert purchase status.
     *
     * @return int
     */
    public function getPurchaseStatusAttribute()
    {
        if ($this->shipped_at) {
            return self::ADVERT_SHIPPED;
        } elseif ($this->purchased_at) {
            return self::ADVERT_PURCHASED;
        } elseif ($this->reserved_at) {
            return self::ADVERT_RESERVED;
        } else {
            return self::ADVERT_AVAILABLE;
        }
    }

    /**
     * Virtual property for displaying advert an purchase status string.
     *
     * @return string
     */
    public function getPurchaseStatusStringAttribute()
    {
        return self::$purchase_statuses[$this->purchase_status];
    }

    /**
     * Check whether there is currently an active transaction for this advert.
     *
     * @return bool
     */
    public function getPurchaseInProgressAttribute()
    {
        $transactions = $this->transactions()->
            where('expires_at', '>=', Carbon::now())->
            where('paypal_status', '=', Transaction::PP_TRANS_CREATED)
            ->get();

        return $transactions->isEmpty() ? false : true;
    }

    /**
     * Return the winning offer, or boolean false if there isn't one.
     *
     * @return Offer
     */
    public function getWinningOfferAttribute()
    {
        $offer = $this->offers()
            ->where('acceptance_status', '=', Offer::OFFER_ACCEPTED)
            ->first();

        return $offer ?: false;
    }

    /**
     * Return the transaction which has a COMPLETED status, or false if there isn't one.
     *
     * @return Transaction
     */
    public function getWinningTransactionAttribute()
    {
        $transaction = $this->transactions()
            ->where('paypal_status', '=', Transaction::PP_TRANS_COMPLETE)
            ->first();

        return $transaction ?: false;
    }

    /**
     * Get first gallery image, or an empty placeholder.
     *
     * @return GalleryPicture
     */
    public function getPrimaryPhotoAttribute()
    {
        if (!$this->photos->isEmpty()) {
            return $this->photos->first();
        } else {
            return new GalleryPicture();
        }
    }

    /**
     * Get the name of the manufacturer, either via a relation or freetext (for 'other').
     *
     * @return string
     */
    public function getManufacturerNameStringAttribute()
    {
        return $this->manufacturer->name == 'Other'
            ? $this->mfr_other_freetext
            : $this->manufacturer->name;
    }

    /**
     * Get a location string for the advert.
     *
     * @return string
     */
    public function getLocationStringAttribute()
    {
        // Retrieve owner's city and country, filtering blank vals
        $location_parts = array_where([
            $this->owner->profile->city,
            $this->owner->profile->telephone_country,
        ], function ($key, $value) {
            return !empty($value);
        });

        // Return joined location elements, or placeholder if none found
        return $location_parts ? implode(', ', $location_parts) : 'Location unknown';
    }

    /**
     * Get a GBP formatted currency string for the price.
     *
     * @return string
     */
    public function getGbpPriceStringAttribute()
    {
        return Converter::to('currency.gbp')->value($this->price)->format();
    }

    /**
     * Get a GBP formatted currency string for the minimum offer price.
     *
     * @return string
     */
    public function getGbpMinOfferPriceStringAttribute()
    {
        return Converter::to('currency.gbp')->value($this->min_offer_price)->format();
    }

    /**
     * Get an array of purchase options which apply to the current user.
     *
     * @return Collection
     */
    public function getUserApplicablePostageOptionsAttribute()
    {
        $applicable_options = new \Collection();
        $postage = $this->postage->keyBy('region_name');

        // Retrieve the collection special case
        if ($postage->has(PostageOption::COLLECT_IN_PERSON)) {
            $applicable_options->push(
                $postage->get(PostageOption::COLLECT_IN_PERSON)
            );
        }

        // Return if not logged in, no profile for user or no country defined
        if (!Confide::user() || !Confide::user()->profile || !Confide::user()->profile->country) {
            return $applicable_options;
        }

        // Retrieve the postage option for this user's profile region
        if ($postage->has(Confide::user()->profile->country->region_name)) {
            $applicable_options->push(
                $postage->get(Confide::user()->profile->country->region_name)
            );
        }

        return $applicable_options;
    }

    /**
     * Check whether the item can be delivered to the current user.
     *
     * @return bool
     */
    public function getIsDeliverableToUserAttribute()
    {
        return $this->getUserApplicablePostageOptionsAttribute()->count() ? true : false;
    }

    //////////////
    // Mutators //
    //////////////

    /**
     * Ensure a 0 value for min offer price is cast to null.
     *
     * @param Decimal
     */
    public function setMinOfferPriceAttribute($value)
    {
        $this->attributes['min_offer_price'] = $value ?: null;
    }

    /**
     * Nullify manufacturer freetext field if 'other' is not selected for manufacturer.
     *
     * @param string $value
     */
    public function setMfrOtherFreetextAttribute($value)
    {
        $manufacturer = Manufacturer::findOrNew($this->manufacturer_id);
        $this->attributes['mfr_other_freetext'] = $manufacturer->name == 'Other' ? $value : null;
    }

    ////////////
    // Scopes //
    ////////////

    /**
     * Restrict to only those items owned by the current member.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeOwnedBySelf($query)
    {
        if (Confide::user()) {
            return $query->where('user_id', '=', Confide::user()->id);
        } else {
            // If not logged in ensure the scope returns nothing
            return $query->whereNull('id');
        }
    }

    /**
     * Restrict to only those items where editing is allowed (not purchased).
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeEditable($query)
    {
        return $query->whereNull('purchased_at')
            ->whereNull('reserved_at')
            ->whereNull('shipped_at');
    }

    /**
     * Restrict to incomplete (not yet shipped) items.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeNotCompleted($query)
    {
        return $query->whereNull('shipped_at');
    }

    /**
     * Restrict to only completed, non editable items (purchased).
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeCompleted($query)
    {
        return $query->whereNotNull('purchased_at')
            ->whereNotNull('reserved_at')
            ->whereNotNull('shipped_at');
    }

    /**
     * Restrict search to include advert suitable for display on frontend.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeFrontendVisible($query)
    {
        return $query->where('approval_status', '=', SELF::ADVERT_APPROVED)
            ->whereNull('shipped_at')
            ->whereNull('purchased_at')
            ->whereNull('reserved_at');
    }

    /**
     * Restrict search to include archived adverts suitable for display on frontend.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeArchivedFrontendVisible($query)
    {
        return $query->where('approval_status', '=', SELF::ADVERT_APPROVED)
            ->whereNotNull('reserved_at');
    }

    /**
     * Restrict search to include items updated within the last month.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeRecentlyModified($query)
    {
        return $query->where('updated_at', '>', Carbon::now()->subMonth());
    }

    /**
     * Scope to limit returned adverts based on the manufacturers filter.
     *
     * @param Query $query
     *
     * @return Query $query
     */
    public function scopeFilterByManufacturer($query)
    {
        // Get persisted searchform
        $searchform_data = Collection::make(Session::get('searchform_data', []));

        if ($searchform_data->has(Manufacturer::SFKEY)) {
            $query->whereHas('manufacturer', function ($query) use ($searchform_data) {
                $query->whereIn('id', $searchform_data->get(Manufacturer::SFKEY));
            });
        }

        return $query;
    }

    /**
     * Scope to limit returned adverts based on the prices filter.
     *
     * @param Query $query
     *
     * @return Query $query
     */
    public function scopeFilterByPrice($query)
    {
        // Get any persisted searchform
        $searchform_data = Collection::make(Session::get('searchform_data', []));

        if ($searchform_data->has(self::SFKEY_PRICE)) {

            // Retrieve prices range string and split into integers for querying
            $prices_range = explode(',', $searchform_data->get(self::SFKEY_PRICE));
            array_walk($prices_range, function (&$value) {
                $value = intval($value);
            });

            $query->whereBetween('price', $prices_range);
        }

        return $query;
    }

    /**
     * Scope to sort the results.
     *
     * @param Query $query
     *
     * @return Query
     */
    public function scopeSorted($query)
    {
        // Get any persisted searchform
        $searchform_data = Collection::make(Session::get('searchform_data', []));

        if ($searchform_data->has(self::SFKEY_SORT)) {
            $selected = (object) self::$sort[$searchform_data->get(self::SFKEY_SORT)];
            $query->orderBy($selected->col, $selected->dir);
        }

        return $query;
    }

    /**
     * Scope to limit results for use in Ajax partials.
     *
     * @param Query $query
     *
     * @return Query
     */
    public function scopeForAjax($query)
    {
        return $query->orderBy('created_at', 'DESC')->limit(10);
    }

    //////////
    // Misc //
    //////////

    /**
     * Cache the result of getDirty() so it can be retreived either side of
     * saving/saved events.
     */
    public function cacheGetDirty()
    {
        $this->cachedDirty = $this->getDirty();
    }

    /**
     * Check the cached dirty array for a SINGLE attribute.
     */
    public function cachedIsDirty($attribute)
    {
        return array_key_exists($attribute, $this->cachedDirty);
    }

    /**
     * Set advert to draft status.
     */
    public function makeDraft()
    {
        $this->approval_status = self::ADVERT_DRAFT;
        $this->save();
    }

    /**
     * Set advert to pending status.
     */
    public function submitForApproval()
    {
        $this->approval_status = self::ADVERT_PENDING;
        $this->save();
    }

    /**
     * Update status and timestamps when shipped.
     *
     * @return bool
     */
    public function markShipped()
    {
        if (!$this->shipped_at) {
            $this->shipped_at = Carbon::now();

            return $this->save();
        } else {
            // Don't update if already shipped
            return false;
        }
    }

    /**
     * Update status and timestamps on cancel, along with corresponding order.
     *
     * @return bool
     */
    public function cancelPendingSale()
    {
        // Can only do this for reserved offers
        if ($this->purchase_status == self::ADVERT_RESERVED) {
            // Status is valid - decline the winning offer and remove reservation timestamp
            $this->fill(['reserved_at' => null])->save();
            $this->winning_offer->fill(['acceptance_status' => Offer::OFFER_SELLER_CANCELLED])->save();

            return true;
        } else {
            return false;
        }
    }
}
