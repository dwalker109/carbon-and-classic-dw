<?php

use Watson\Validating\ValidatingTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class GalleryPicture extends Eloquent implements StaplerableInterface
{
    use ValidatingTrait;

    use EloquentTrait;

    protected $fillable = ['image', 'sequence'];

    protected $rules = [
        'image' => ['stapler_image'],
    ];


    ///////////////
    // Bootstrap //
    ///////////////


    public function __construct(array $attributes = array())
    {
        // Add image Stapler support
        $this->hasAttachedFile('image', [
            'styles' => [
                'massive' => '1200x900#',
                'huge' => '900x675#',
                'large' => '600x450#',
                'medium' => '300x225#',
                'thumb' => '100x75#',
            ]
        ]);

        parent::__construct($attributes);
    }


    public function getDates()
    {
        return ['created_at', 'updated_at', 'image_updated_at'];
    }


    ///////////////////
    // Relationships //
    ///////////////////


    /**
     * Define polymorphic relationship
     *
     * @return EloquentInstance (Varies)
     */
    public function imageable()
    {
        return $this->morphTo();
    }
}
