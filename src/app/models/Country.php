<?php

class Country extends Eloquent
{
    public $timestamps = false;


    ///////////////
    // Utilities //
    ///////////////


    /**
     * Return an array of all countries for use in dropdowns, via cache
     *
     * @return array
     */
    public static function getList()
    {
        return Cache::remember('countries_dropdown', 60, function() {
            return self::all()->lists('country_name', 'alpha_2_code');
        });
    }


    /**
     * Return an array of all valid regions, via cache
     *
     * @return array
     */
    public static function getRegions()
    {
        return Cache::remember('regions', 60, function() {
            $regions = self::where('region_name', '!=', '')->distinct()
                ->orderBy('region_name')->lists('region_name');

            // Add the collect in person option
            array_unshift($regions, PostageOption::COLLECT_IN_PERSON);

            return $regions;
        });
    }


    /**
     * Return an array of all valid subregions, via cache
     *
     * @return array
     */
    public static function getSubRegions()
    {
        return Cache::remember('sub_regions', 60, function() {
            $regions = self::where('sub_region_name', '!=', '')->distinct()
                ->orderBy('sub_region_name')->lists('sub_region_name');

            // Add the collect in person option
            array_unshift($regions, PostageOption::COLLECT_IN_PERSON);

            return $regions;

        });
    }
}
