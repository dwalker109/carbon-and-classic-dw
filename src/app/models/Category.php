<?php

use Baum\Node;
use Watson\Validating\ValidatingTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Illuminate\Support\Collection;

class Category extends Node implements SluggableInterface, StaplerableInterface
{

    use ValidatingTrait;

    use SoftDeletingTrait;

    use SluggableTrait;

    use EloquentTrait;

    protected $fillable = [
        'name',
        'description',
        'active',
        'is_advert_container',
        'parent_id',
        'image',
     ];

    protected $sluggable = [
        'build_from' => 'name',
    ];

    protected $rules = [
        'parent_id' => ['integer'],
        'name' => ['required', 'max:255'],
        'description' => ['max:255'],
        'active' => ['integer'],
        'is_advert_container' => ['integer'],
        'image' => ['stapler_image'],
    ];

    ///////////////
    // Bootstrap //
    ///////////////


    public function __construct(array $attributes = array())
    {
        // Add image Stapler support
        $this->hasAttachedFile('image', [
            'styles' => [
                'huge' => '900x675#',
                'large' => '600x450#',
                'medium' => '300x225#',
                'thumb' => '100x75#',
            ]
        ]);

        parent::__construct($attributes);
    }


    public static function boot()
    {
        parent::boot();
        static::bootStapler();

        // Add handler to deal with  nested set moves prior to the rest of the save
        Category::updating(function ($category) {
            if ($category->isDirty('parent_id')) {
                return self::handleMoves($category);
            }
        });
    }


    ///////////////////
    // Relationships //
    ///////////////////


    /**
     * Define hasMany relationship with adverts
     *
     * @return Collection
     */
    public function adverts()
    {
        return $this->hasMany('Advert');
    }


    ////////////
    // Scopes //
    ////////////


    /**
     * Scope to limit results to categories which are designated as
     * containers for adverts
     * 
     * @param Query $query
     * @return Query
     */
    public function scopeIsAdvertContainer($query)
    {
        return $query->where('is_advert_container', '=', true);
    }


    ///////////////
    // Accessors //
    ///////////////


    /**
     * Return the next level of categories, unfiltered
     *
     * @return Collection
     */
    public function getSubcategoriesAttribute()
    {
        return $this->getImmediateDescendants();
    }


    /**
     * Return adverts in this category, filtered
     *
     * @return Collection
     */
    public function getFilteredAdvertsAttribute()
    {
        return $this->adverts()->frontendVisible()->filterByManufacturer()
            ->filterByPrice()->sorted()->get();
    }


    /**
     * Return the immediate parent category
     *
     * @return Category
     */
    public function getParentCategoryAttribute()
    {
        return $this->parent()->first();
    }


    //////////////
    // Mutators //
    //////////////


    /**
     * Save any 0 or otherwise non integer vals as null to avoid nested set issues
     */
    public function setParentIdAttribute($value)
    {
        $value = intval($value) === 0 ? null : $value;
        $this->attributes['parent_id'] = $value;
    }


    //////////
    // Misc //
    //////////


    /**
     * Handle moving categories in the hierachy and catch errors before they
     * break the nested set tree - called from the saving event
     *
     * @param  Category $category
     * @return boolean
     */
    private static function handleMoves($category)
    {
        // Catch exceptions thrown by impossible moves
        try {
            if ($category->parent_id == 0) {
                // No parent - make a root
                $category->makeRoot();
                return true;
            } else {
                // Some other parent selected - try to move and catch exceptions
                $parent = Category::find($category->parent_id);
                $category->makeChildOf($parent);
                return true;
            }
        } catch (Baum\MoveNotPossibleException $e) {
            // Move failed - add notification and cancel save
            Notification::error('Could not move category');
            return false;
        }
    }


    /**
     * Build a custom nested list array with the disabled attrib for categories
     * which are not advert containers, so they cannot be selected.
     *
     * @return array
     */
    public static function getCustomNestedList()
    {
        $advert_containers = Category::isAdvertContainer()->get();
        $full_nested_list = Category::getNestedList('name', null, '&rArr;');
        $new_nested_list = [];

        foreach ($full_nested_list as $id => $name) {
            $new_nested_list[$id] = [
                'value' => $id,
                'text' => $name,
                'disabled' => $advert_containers->contains($id) ? false : true,
            ];
        }

        return $new_nested_list;
    }


    /**
     * Build a custom nested list array including heirarchy (no need to 
     * set any as disabled here)
     *
     * @return array
     */
    public static function getFullHierachyNestedList()
    {
        $list = [];

        foreach (Category::all() as $category) {

            $hierachy = null;

            foreach ($category->getAncestors() as $ancestor) {
                $hierachy .= "{$ancestor->name} &gt; ";
            }

            $list[$category->id] = "{$hierachy}{$category->name}";

/*            $list[$category->id] = [
                'value' => $category->id,
                'text' => $hierachy . $category->name,
                'disabled' => $advert_containers->contains($category->id) ? false : true,
            ];
*/
        }

        return $list;
        
    }
}
