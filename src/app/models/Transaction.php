<?php

class Transaction extends Eloquent
{

    const PP_TRANS_COMPLETE = 'COMPLETED';
    const PP_TRANS_CREATED = 'CREATED';
    const PP_TRANS_CANCELLED = 'CANCELLED';



    use SoftDeletingTrait;

    protected $fillable = [
        'purchase_price',
        'paypal_tracking_id',
        'paypal_status',
        'paypal_sender_email',
        'expires_at',
    ];

    protected $dates = [
        'expires_at',
    ];


    ///////////////
    // Bootstrap //
    ///////////////

    /**
     * Boot the parent, register observer
     */
    public static function boot()
    {
        parent::boot();

        Transaction::observe(new TransactionObserver);
    }


    ///////////////////
    // Relationships //
    ///////////////////

    /**
     * BelongsTo relationship with source advert
     *
     * @return Advert
     */
    public function advert()
    {
        return $this->belongsTo('Advert');
    }

    /**
     * BelongsTo relationship with buying user
     *
     * @return User
     */
    public function buyer()
    {
        return $this->belongsTo('User', 'buyer_id');
    }


    ///////////////
    // Accessors //
    ///////////////
    

    /**
     * Get a GBP formatted currency string for the price
     *
     * @return String
     */
    public function getGbpPurchasePriceStringAttribute()
    {
        return Converter::to('currency.gbp')->value($this->purchase_price)->format();
    }
}
