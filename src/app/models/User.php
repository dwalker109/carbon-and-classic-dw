<?php

use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;
use Zizaco\Entrust\HasRole;
use Cmgmyr\Messenger\Traits\Messagable;


class User extends Eloquent implements ConfideUserInterface
{
    use ConfideUser;
    use HasRole;
    use Messagable;


    ///////////////////
    // Relationships //
    ///////////////////


    /**
     * Define relationship to Profile
     *
     * @return  Profile
     */
    public function profile()
    {
        return $this->hasOne('Profile');
    }


    /**
     * Define relationship with any advert purchases
     *
     * @return Collection
     */
    public function transactions()
    {
        return $this->hasMany('Transaction', 'buyer_id');
    }


    /**
     * Define relationship with any advert offers made
     *
     * @return Collection
     */
    public function offersMade()
    {
        return $this->hasMany('Offer', 'initiator_id')->orderBy('created_at', 'desc');
    }


    /**
     * Define relationship with any offers received
     *
     * @return Colection
     */
    public function offersReceived()
    {
        return $this->hasManyThrough('Offer', 'Advert')->orderBy('created_at', 'desc');
    }


    /**
     * Define relationship with any activities
     *
     * @return Collection
     */
    public function activities()
    {
        return $this->hasMany('Activity')->orderBy('created_at', 'desc');
    }


    ///////////////
    // Accessors //
    ///////////////


    /**
     * Add virtual property to query whether Profile completed
     *
     * @return  Boolean
     */
    public function getIsProfileHolderAttribute()
    {
        // Check existence of dynamic profile property defined above
        return $this->profile ? true : false;
    }


    /**
     * Add virtual property to query whether Profile has a PayPal ID assigned
     *
     * @return  Boolean
     */
    public function getIsExtendedProfileHolderAttribute()
    {
        // Check existence of dynamic profile property defined above, and whether
        // the user has set their PayPal ID in it
        return $this->profile && $this->profile->paypal_id ? true : false;
    }


    /**
     * Add virtual property for listing user type
     *
     * @return  String
     */
    public function getRolesListAttribute()
    {
        $roles_list = $this->roles()->getResults()->lists('name');
        return $roles_list ? implode(', ', $roles_list) : 'Deactivated';
    }


    /**
     * Add a virtual property to see if any new activities or messages exist
     *
     * @return boolean
     */
    public function hasNewActivities()
    {
        if ($this->newMessagesCount() || $this->activities()->unread()->count()) {
            return true;
        } else {
            return false;
        }
    }


    //////////////
    // Mutators //
    //////////////


    /**
     * Set base user role to (exactly) one of the predefined types
     *
     * @param   String $role
     * @return  void
     */
    public function setBaseRole($required_role)
    {
        // Remove all base role
        foreach (Role::$base_roles as $base_role) {
            $role = Role::where('Name', '=', $base_role)->first();
            $this->detachRole($role);
        }

        // Attach the required role
        $role = Role::where('Name', '=', $required_role)->first();
        $this->attachRole($role);
    }


    //////////
    // Misc //
    //////////


    /**
     * Deactivate account (remove all roles)
     *
     * @return void
     */
    public function deactivate()
    {
        $this->roles()->sync([]);
    }
}
