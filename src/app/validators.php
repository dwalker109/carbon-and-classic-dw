<?php

/*
|--------------------------------------------------------------------------
| Custom Validation
|--------------------------------------------------------------------------
|
| Define some extra systemwide validators here
|
*/

/**
 * Check input is alphabetic chars, spaces allowed
 *
 * @param String $attribute
 * @param String $value
 *
 * @return Boolean
 */
Validator::extend('alpha_spaces', function($attribute, $value) {
    return preg_match('/^([-a-z0-9_\s-])+$/i', $value);
});


/**
 * Extend the mimetype validator to work around Stapler objects - substitutes
 * in the $_FILES data instead, and validates that
 *
 * @param String $attribute
 * @param String $value
 * @param Array $parameters
 *
 * @return Boolean
 */
Validator::extend(
    'stapler_mimes',
    function($attribute, $value, $parameters) {
        
        $validator = Validator::make(
            [$attribute => Input::file($attribute)],
            [$attribute => 'mimes:' . implode(',', $parameters)]
        );

        return $validator->passes();
    }
);


/**
 * Extend the image validator to work around Stapler objects - substitutes
 * in the $_FILES data instead, and validates that
 *
 * @param String $attribute
 * @param String $value
 *
 * @return Boolean
 */
Validator::extend(
    'stapler_image',
    function($attribute, $value) {
        
        $validator = Validator::make(
            [$attribute => Input::file($attribute)],
            [$attribute => 'image']
        );

        return $validator->passes();
    }
);


/**
 * Check input is a percentage of another field
 *
 * @param String $attribute
 * @param String $value
 * @param Array $parameters
 * @param Validator $validator
 *
 * @return Boolean
 */
Validator::extend(
    'is_percentage_of',
    function($attribute, $value, $parameters, $validator) {

        // Convert params into a more usable format
        $parameters = (object) [
            'compare_attribute' => (object)
                ['name' => $parameters[0], 'value' => $validator->getData()[$parameters[0]]],
            'min_percent' => $parameters[1],
            'max_percent' => $parameters[2],
        ];

        // Calculate comparison value boundaries
        $compare_range = (object) [
            'lower' => floor(($parameters->min_percent / 100) * $parameters->compare_attribute->value),
            'upper' => ceil(($parameters->max_percent / 100) * $parameters->compare_attribute->value),
        ];

        // Calculate and return result
        return $value >= $compare_range->lower && $value <= $compare_range->upper;
    }
);


/**
 * Custom error message for is_percentage_of validator
 *
 * @param String $rule
 * @param Closure $replacer
 *
 * @return String
 */
Validator::replacer('is_percentage_of', function($message, $attribute, $rule, $parameters) {
    return str_replace(
        [':compare_attribute', ':lower', ':upper'],
        [$parameters[0], $parameters[1], $parameters[2]],
        $message
    );
});
