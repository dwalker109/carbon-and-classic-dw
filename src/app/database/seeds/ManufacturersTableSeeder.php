<?php

class ManufacturersTableSeeder extends Seeder
{

    public function run()
    {
        Manufacturer::create([
            'name' => 'Other',
            'description' => 'DO NOT MODIFY THIS ENTRY IN ANY WAY!!!',
        ]);
    }
}
