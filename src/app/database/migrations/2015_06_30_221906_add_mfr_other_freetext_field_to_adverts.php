<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMfrOtherFreetextFieldToAdverts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('adverts', function(Blueprint $table)
		{
			$table->string('mfr_other_freetext');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('adverts', function(Blueprint $table)
		{
			$table->dropColumn('mfr_other_freetext');
		});
	}

}
