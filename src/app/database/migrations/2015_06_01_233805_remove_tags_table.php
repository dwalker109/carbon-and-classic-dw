<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveTagsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::drop('tags');
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active');
            $table->string('name', 255);
            $table->string('description', 255);
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }
}
