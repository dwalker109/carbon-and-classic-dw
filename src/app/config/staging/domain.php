<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Domain specific settings
    |--------------------------------------------------------------------------
    |
    | Add any miscellaneous domain specific config values here
    |
    */

    // Map a role display name (case must match) to a generic email address
    'role_email' => [

        'Admin' => 'carbonandclassic@gmail.com',
        'Member' => 'carbonandclassic@gmail.com',

    ]
);
