<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | PayPal
    |--------------------------------------------------------------------------
    |
    | Credentials for dealing with PayPal are specified here
    |
    */


    // Email address of PayPal account to receive commission
    'email' => 'carbonclassic-site@gmail.com',
    
    // Full PayPal Adaptive Payment endpoint URL, ready to append a payKey
    'auth_url' => 'https://www.sandbox.paypal.com/webscr?cmd=_ap-payment&paykey=',

    // URL used to receive PayPal IPN messages
    'ipn_url' => 'http://46.101.8.44/ipn',

    // Duration allowed for a checkout to complete, as an XSD 'Duration' string.
    // See http://www.w3schools.com/schema/schema_dtypes_date.asp for details
    'pay_key_duration' => 'PT5M',

    // PayPal SDK global config
    'sdk_config' => [
        'mode' => 'sandbox',
        'acct1.UserName' => 'carbonclassic-site_api1.gmail.com',
        'acct1.Password' => 'TLK7K7TTTKDNM3JW',
        'acct1.Signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AYKiYFobdX7om.et5X.SAKFVZfDw',
        'acct1.AppId' => 'APP-80W284485P519543T',
    ],

);
