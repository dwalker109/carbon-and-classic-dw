<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Mail Driver
    |--------------------------------------------------------------------------
    |
    | Laravel supports both SMTP and PHP's "mail" function as drivers for the
    | sending of e-mail. You may specify which one you're using throughout
    | your application here. By default, Laravel is setup for SMTP mail.
    |
    | Supported: "smtp", "mail", "sendmail", "mailgun", "mandrill", "log"
    |
    */

    // Config for Mailcatcher (view mails on <host>:10800)

    'driver' => 'smtp',
    'host' => 'mailtrap.io',
    'port' => 2525,
    'from' => array(
        'address' => 'from@example.com',
        'name' => 'Example',
    ),
    'username' => '1bd5469edae8b7',
    'password' => '4bafca872aa60d',
    'sendmail' => '/usr/sbin/sendmail -bs',
    'pretend' => false,
);
