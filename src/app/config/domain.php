<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Domain specific settings
    |--------------------------------------------------------------------------
    |
    | Add any miscellaneous domain specific config values here
    |
    */

    'site' => [

        'default_page_title' => 'Vintage and modern cycles and components',
        'default_meta_description' => 'The web\'s best place to sell your '
            . 'vintage and modern cycles and components. Listing is free, '
            . 'with a simple fixed fee on successful sales.',

    ],

    'advert' => [

        'min_price' => 1,
        'max_price' => 99999.99,
        'min_offer_price_percentage' => 50,
        'max_gallery_images' => 10,

        // Skip approval for advert main data edits where ONLY these fields change
        'skip_approval_whitelist' => [
            'price',
            'min_offer_price',
        ],

    ],

    'postage' => [

        'min_price' => 0,
        'max_price' => 999.99,

    ],

    'profile' => [

        // Set any default values here for a dummy Profile instance
        'dummy' => [
            'full_name' => 'C&C Member',
        ],

    ],

    // Map a role display name (case must match) to a generic email address
    'role_email' => [

        'Admin' => 'admin@carbon-and-classic.app',
        'Member' => 'webmaster@carbon-and-classic.app',

    ]
);
