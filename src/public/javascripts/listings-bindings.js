/**
 * Bind jQuery functionality specific to listings pages
 */

$(document).ready(function() {

	// Handle filter show hide - only run if the filters div is present
	if ($('#category-filters').length) {

		// Set up show/hide classes dependent on filters
		restore_filter_classes();

		// Show and hide filters on button click
		$('#toggle-filters-button').click(function() {

			// Fade out, then do the DOM work...
			$('#category-inner-wrapper').fadeOut('slow', function() {

				// Toggle classes
				$('#category-filters').toggleClass('hidden');
				$('#category-main')
					.toggleClass('category-main-filters-enabled')
					.toggleClass('category-main-filters-hidden');
				$('.listing-item')
					.toggleClass('listing-item-filters-enabled')
					.toggleClass('listing-item-filters-hidden');

				// Store set classes for next load
				save_filter_classes();

				// Fade in again and trigger a resize (to fix equal height rows)
				$('#category-inner-wrapper').fadeIn('slow', function() {
					$(window).trigger('resize');
				});
			});
		});
	}

	// Register global Ajax events to handle page transitions
	$(document).ajaxStart(function() {
		$('#category-main').fadeOut(200);
	});
	$(document).ajaxStop(function() {
		$('#category-main').fadeIn(200);

		// Setup equal height rows on loaded content and force resize
		$('.equal-height').responsiveEqualHeightGrid();	
		$(window).trigger('resize');
	});

	// Auto submit form on change, or on slider mouseup
	$('#filters-form').change(function() {
		$('#filters-form').submit(); 
	});

	// Capture filter submissions and submit via ajax (through UnderscoreJS debounce)
	var throttledSubmit = _.debounce(search_submit, 500);
	$('#filters-form').submit(function(event) {

		event.preventDefault();
		throttledSubmit();

	});


	/////////////////////
	// Utility classes //
	/////////////////////


	/**
	 * Save CSS in use for layout of grid classes, based on filters being shown or hidden
	 */
	function save_filter_classes() {
		localStorage.setItem('category-filters-classes', $('#category-filters').attr('class'));
		localStorage.setItem('category-main-classes', $('#category-main').attr('class'));
		localStorage.setItem('listing-item-classes', $('.listing-item').first().attr('class'));
	}


	/**
	 * Restore CSS in use for layout of grid classes, based on filters being shown or hidden
	 */
	function restore_filter_classes() {
		$('#category-filters').attr('class', localStorage.getItem('category-filters-classes')
			|| $('#category-filters').attr('class'));
		$('#category-main').attr('class', localStorage.getItem('category-main-classes') 
			|| $('#category-main').attr('class'));
		$('.listing-item').attr('class', localStorage.getItem('listing-item-classes') 
			|| $('.listing-item').first().attr('class'));
	}


	/**
	 * Submit the search form and repopulate grid
	 */
	function search_submit() {

		// UI niceness
		$('#filters-progress-bar').fadeIn();

		// Send the form via AJAX		
		$filterForm = $('#filters-form');
		$.ajax({
			type: $filterForm.attr('method'),
			url: $filterForm.attr('action'),
			data: $filterForm.serialize(),
			success: function(data) {
				$('#category-main').html(data);
				restore_filter_classes();
				$('#filters-progress-bar').fadeOut();
			}
		});
	}

});