/**
 * Bind jQuery functionality to page elements
 */

$(document).ready(function() {

	// Homepage image
	$('div.home-wrapper').parents('html').backstretch("/images/vista-1.jpg");

	// bxSlider
	var slider = $('.bxslider').bxSlider({
		mode: 'fade',
		adaptiveHeight: true,
		pagerCustom: '#bx-pager',
		prevSelector: '#bx-prev',
		prevText: '<span class="glyphicon glyphicon-menu-left"> </span>',
		nextSelector: '#bx-next',
		nextText: '<span class="glyphicon glyphicon-menu-right"> </span>',
		onSlideAfter: function() {

			// Get index and counter, reset 'current' to 1 indexed
		    var current = 1 + slider.getCurrentSlide();
		    var count = 0 + slider.getSlideCount();
		    
		    // Set the counter text
			$('#bx-counter').text(current + '/' + count);
			
			// Update the displayed magnify link
			$('.bx-magnify-links').removeClass("hidden").addClass("hidden");
			$('#bx-magnify-link-' + slider.getCurrentSlide()).removeClass("hidden");
	  	}
	});

	// Clicking a bxSlider image will fake the "next" click
	$('.bx-viewport').click(function() {
		slider.goToNextSlide();
	})

	// Init bootstrap-slider for search filters
	$('#searchfilter-slider').slider({
		// Defaults
		tooltip: 'hide',
		step: 50,
		// Set the slider value as an array, from hidden fields
		value: [
			parseInt($('#searchfilter-start-price').val()),
			parseInt($('#searchfilter-end-price').val())
		]
	});

	// Update start/end hidden fields and label text on slider change
	$("#searchfilter-slider").on("change", function(slideEvt) {

		var startPrice = slideEvt.value.newValue[0].toString();
		var endPrice = slideEvt.value.newValue[1].toString();

		// Hidden fields
		$('#searchfilter-start-price').val(startPrice);
		$('#searchfilter-end-price').val(endPrice);
		
		// Label
		$('#searchfilter-start-price-label').text(startPrice);
		$('#searchfilter-end-price-label').text(endPrice);
	});

});
