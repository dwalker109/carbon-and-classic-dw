/**
 * Bind jQuery functionality to page elements
 */

$(document).ready(function() {

    // Select2
    $('select').not('.not-select2').select2({
    	placeholder: 'Please select',
    	minimumResultsForSearch: 10
    });

	// Bootbox popups
	$(document).on("click", ".bootbox-popup", function(e) {
		e.preventDefault();
		bootbox.alert($(this).data("message"));
	});
	
    // Bootstrap 3 wysihtml5 text editor
    $('textarea').not('.textarea-simple').wysihtml5({
    	'toolbar': {
	    	'blockquote': false,
	    	'image': false
    	}
    });
    // Need to manually remove the indent and outdent as they
    // live in the same block as bullet/number lists
    $('*[data-wysihtml5-command="Outdent"]').remove();
    $('*[data-wysihtml5-command="Indent"]').remove();

    // wysiwyhtml5 editor without any buttons, just sandboxing
    $('textarea.textarea-simple').wysihtml5({
		'toolbar': {
			"font-styles": false,
			"emphasis": false,
			"lists": false,
			"html": false, 
			"link": false,
			"image": false,
			"color": false, 
			"blockquote": false,
		}
	});

	// Dismissing notifications
	window.setTimeout(function() {
		$("#messages-popup").fadeTo(500, 0).slideUp(500, function(){
			$(this).remove(); 
		});
	}, 5000);

	// Setup equal height rows
	$('.equal-height').responsiveEqualHeightGrid();	
	
	// Setup popovers
	$('.show-popover').popover({
		toggle: 'popover',
		html: true,
		placement: 'bottom'
	});

	// Make sure only one popover exists at a time
	$('.show-popover').click(function(){
	    $('.show-popover').not(this).popover('hide'); //all but this
	});

	// Turn these select boxes into a set of heriarchical ones
	$('#category_id.hierarchy').selectHierarchy();

	// These category lists will load with their id number displayed as their text, and
	// their friendly name in a 'text' attribute (this is due to a limitation in
	// the lib used to generate them) - fix this here
	$('#category_id.fix-integer-labels > option').each( function() {
		$(this).text($(this).attr('text'));
	});

	// Hide the "other" freetext field for manufacturers unless "other" is picked
	$('#manufacturer_id').change( function() {
		if ($(this).find("option:selected").text() == 'Other') {
			$('#mfr_other_freetext_container').show();
		} else {
			$('#mfr_other_freetext_container').hide();
		}
	});
	$('#manufacturer_id').change();

	// Open Bootbox modal for listings preview pages
	$(document).on("click", ".show-listing-preview-modal", function(e) {
		e.preventDefault();
		var iframe_src = $(this).attr('href');
		console.debug(iframe_src);
		bootbox.dialog({
			size: 'large',
			message: '<iframe src="' + iframe_src + '" frameBorder="0" width=100% height=500></iframe>',
			buttons: {
			    main: {
			        label: "Close",
			        className: "btn-primary",
			        callback: bootbox.hideAll(),
			    }
			},
			closeButton: false,
		});
    });    


});

